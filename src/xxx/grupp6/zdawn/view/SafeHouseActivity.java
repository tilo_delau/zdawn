package xxx.grupp6.zdawn.view;

import java.io.IOException;

import xxx.grupp6.zdawn.R;
import xxx.grupp6.zdawn.model.SoundHandler;
import xxx.grupp6.zdawn.view.memberdata.LocationHandler;
import xxx.grupp6.zdawn.view.memberdata.PlayerInfo;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;

/**
 * SafeHouse is the place to rest between gamelevels. It shows the players current info like
 * name, score, level and location. This is also the place where the new earned score is added
 * to playerInfo and database.
 * 
 * @author Daniel
 * 
 */
public class SafeHouseActivity extends Activity {
	private PlayerInfo pi;
	private LocationHandler location;
	private boolean isDead;
	private int zombieKillScore;
	private int molotovKillScore;
	private int gameLevel;
	private boolean showLocation = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// location = new LocationHandler(this);
		// location.addPropertyChangeListener(this);
		setContentView(R.layout.activity_safe_house);
		removeActionBar();
		pi = new PlayerInfo(this);
		location = LocationHandler.getInstance(this);
		pi.setLocation(location.getCity());
		Intent intent = getIntent();
		processIntentValue(intent);
		Button butt = (Button) findViewById(R.id.safe_continue_game_button);
		butt.setOnClickListener(buttonListener);
		butt = (Button) findViewById(R.id.safe_options_menu_button);
		butt.setOnClickListener(buttonListener);
		butt = (Button) findViewById(R.id.safe_main_menu_button);
		butt.setOnClickListener(buttonListener);

		showLocation = showLocation();
		writeText();
		blink();

	}

	private void processIntentValue(Intent intent) {
		isDead = intent.getBooleanExtra("isDead", false);
		TextView view = (TextView) findViewById(R.id.level_done_text);
		Button playButton = (Button) findViewById(R.id.safe_continue_game_button);
		if (isDead) {
			view.setText("You died !!\n\nYour stats:\nName...\nScore...\nLevel...\nLocation...");
			playButton.setText("Play again...");
		} else {
			view.setText("WELL DONE!\n\nYour stats:\nName...\nScore...\nLevel...\nLocation...");
			playButton.setText("Continue game...");
			zombieKillScore = intent.getIntExtra("zombieKillScore", 0);
			molotovKillScore = intent.getIntExtra("molotovKillScore", 0);
			gameLevel = intent.getIntExtra("completedLevel", pi.getGameLevel());
			pi.addScore(zombieKillScore + molotovKillScore);
			pi.setGameLevel(gameLevel);
		}
		Log.d("datta", "SH4: Zombiescore " + zombieKillScore + "mol: "
				+ molotovKillScore + " gameLevel " + gameLevel);
		Log.d("datta", "SH5  getintent " + pi.getScore());

	}

	private boolean showLocation() {
		SharedPreferences sh = this.getSharedPreferences(
				"xxx.grupp6.zdawn.SETTING", Context.MODE_PRIVATE);
		return sh.getBoolean("showLocation", true);
	}

	private void writeText() {
		Log.d("datta",
				"WriteText:\nName: " + pi.getName() + " Score: "
						+ pi.getScore() + "\nLevel: " + pi.getGameLevel()
						+ " Location: " + pi.getLocation());

		String location = "";
		if (showLocation) {
			location = pi.getLocation();
		} else {
			location = "Ankeborg";
		}
		;
		TextView view = (TextView) findViewById(R.id.level_info_text);
		String playerText = "\n\n\n" + pi.getName() + "\n" + pi.getScore()
				+ "\n" + pi.getGameLevel() + "\n" + location;
		view.setText(playerText.replace("\\n", "\n"));
	}

	private void blink() {

		Button v = (Button) findViewById(R.id.safe_continue_game_button);
		Animation anim = new AlphaAnimation(1.0f, 0.0f);
		anim.setDuration(400); // You can manage the blinking time with this
								// parameter
		anim.setStartOffset(500);
		anim.setRepeatMode(Animation.REVERSE);
		anim.setRepeatCount(Animation.INFINITE);
		v.startAnimation(anim);

	}

	private void removeActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.hide();
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}

	private void menuOptions(View v) {
		switch (v.getId()) {
		case R.id.safe_continue_game_button: {
			Intent intent = new Intent(this, GamePlayActivity.class);
			intent.putExtra("gameLevel", gameLevel);
			startActivity(intent);
			finish();
			try {
				SoundHandler.getMusic(this, 3);
			} catch (IOException e) {
				e.printStackTrace();
			}
			break;
		}
		case R.id.safe_options_menu_button: {
			Intent intent = new Intent(this, GameOptionsActivity.class);
			startActivity(intent);
			finish();
			break;
		}
		case R.id.safe_main_menu_button: {
			Intent intent = new Intent(this, MainMenuActivity.class);
			startActivity(intent);
			finish();
			break;
		}
		}
	}

	private OnClickListener buttonListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			menuOptions(v);
		}
	};

}
