package xxx.grupp6.zdawn.view;

import xxx.grupp6.zdawn.R;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;

/**
 * 
 * @author Tilo,2014
 * Just a little animation something about grupp 6
 *
 */
public class AboutUsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_us);
		removeActionBar();
		Button button = (Button) findViewById(R.id.return_button);
		button.setOnClickListener(buttonListener);
		
		final TextView tv1 = (TextView) findViewById(R.id.david);
		final TextView tv2 = (TextView) findViewById(R.id.tilo);
		final TextView tv3 = (TextView) findViewById(R.id.daniel);
		final TextView tv4 = (TextView) findViewById(R.id.markus);
			
		tv1.setAlpha(0f);
		tv2.setAlpha(0f);
		tv3.setAlpha(0f);
		tv4.setAlpha(0f);
		
		tv1.setScaleX(9);
		tv1.setScaleY(9);
		tv2.setScaleX(9);
		tv2.setScaleY(9);
		tv3.setScaleX(9);
		tv3.setScaleY(9);
		tv4.setScaleX(9);
		tv4.setScaleY(9);
				
		final ViewPropertyAnimator animatorTv1 = tv1.animate();
		final ViewPropertyAnimator animatorTv2 = tv2.animate();
		final ViewPropertyAnimator animatorTv3 = tv3.animate();
		final ViewPropertyAnimator animatorTv4 = tv4.animate();
		
		Runnable endActionText = new Runnable() {
			
		     public void run() {
		    	animatorTv1.translationX(-200);
		    	animatorTv2.translationX(200);
		    	animatorTv3.translationX(-200);
		    	animatorTv4.translationX(200);
		    	
		    	animatorTv1.translationY(50);
		    	animatorTv2.translationY(100);
		    	animatorTv3.translationY(50);
		    	animatorTv4.translationY(100);
		    	
		    	animatorTv3.rotation(360);
		    	
				
				animatorTv1.setDuration(4000);
				animatorTv2.setDuration(4000);
				animatorTv3.setDuration(4000);
				animatorTv4.setDuration(4000);

				animatorTv1.setStartDelay(1000);
				animatorTv2.setStartDelay(2000);
				animatorTv3.setStartDelay(3000);
				animatorTv4.setStartDelay(4000);
				
				blink();

		        Log.d("About","End of credits animation");
		     }
		 };
		 
		animatorTv1.alpha(1f);
		animatorTv2.alpha(1f);
		animatorTv3.alpha(1f);		
		animatorTv4.alpha(1f);
		
		animatorTv1.scaleX(1);
		animatorTv1.scaleY(1);
		animatorTv2.scaleX(1);
		animatorTv2.scaleY(1);
		animatorTv3.scaleX(1);
		animatorTv3.scaleY(1);
		animatorTv4.scaleX(1);
		animatorTv4.scaleY(1);
		
		animatorTv1.setDuration(4000);
		animatorTv2.setDuration(4000);
		animatorTv3.setDuration(4000);
		animatorTv4.setDuration(4000);
		
		animatorTv1.setStartDelay(2000);
		animatorTv2.setStartDelay(3000);
		animatorTv3.setStartDelay(4000);
		animatorTv4.setStartDelay(5000);

		
		animatorTv1.start();
		animatorTv2.start();
		animatorTv3.start();
		animatorTv4.withEndAction(endActionText); // need the last one to call back
		animatorTv4.start();
		
		tv1.bringToFront();
		tv2.bringToFront();
		tv3.bringToFront();
		tv4.bringToFront();
		
	}

	private void blink() {

		Button v = (Button) findViewById(R.id.play_button);
		v.setVisibility(View.VISIBLE);
		v.bringToFront();
		v.setOnClickListener(buttonListener);
		Animation anim = new AlphaAnimation(1.0f, 0.0f);
		anim.setDuration(400); // You can manage the blinking time with this parameter
		anim.setStartOffset(500);
		anim.setRepeatMode(Animation.REVERSE);
		anim.setRepeatCount(Animation.INFINITE);
		v.startAnimation(anim);

	}
	private void menuOptions(View v) {
		switch (v.getId()) {
		case R.id.return_button: {
			Intent intent = new Intent(this, MainMenuActivity.class);
			startActivity(intent);
			break;
		}
		}
	}
	private void removeActionBar() {
        //Get rid of ActionBar
        ActionBar  actionBar = getActionBar();
        actionBar.hide();
        //Get rid of notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.about_us, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private OnClickListener buttonListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			menuOptions(v);
		}
	};
}
