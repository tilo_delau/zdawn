package xxx.grupp6.zdawn.view;

import java.beans.PropertyChangeEvent;


import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import xxx.grupp6.zdawn.R;
import xxx.grupp6.zdawn.model.GameEngine;
import xxx.grupp6.zdawn.model.SoundHandler;
import xxx.grupp6.zdawn.model.zombies.AbstractZombie;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;

import android.widget.RelativeLayout;
import android.widget.TextView;
/**
 * 
 * @author Tilo 2014
 * GamePlayActivity is the main scene of ZDawn and takes care of most of the android elements
 * with calls to other classes in the game.
 * We set up the players views, listen to GE engine and add zombies accordingly
 *
 */
public class GamePlayActivity extends Activity implements PropertyChangeListener{

	private SharedPreferences sp;
	private GameEngine ge;
	private TextView hpTv;
	private TextView ammoTv;
	private ImageView glock9mmV;
	private ImageView molotovV;
	private ImageView shotgunV;	
	private boolean isGlock = true;
	private int map;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        
        setContentView(R.layout.activity_game_play);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        sp = getSharedPreferences("xxx.grupp6.zdawn.SETTING",Context.MODE_PRIVATE);
        ge = GameEngine.getInstance();
        ge.newGame();
        Log.d("markus", "zkillscore: "+ge.getZombieKillScore());
        ge.addPropertyChangeListener(this);
        ge.setZombieSpawnTimer(sp.getInt("level", 1));
        ge.startZombieSpawnTimer();
        
		ImageView iPipe = (ImageView) this.findViewById(R.id.obstacle_pipe);
		iPipe.setVisibility(View.INVISIBLE);
        
        if(sp.getInt("level", 1) % 2 == 0) {
    		RelativeLayout lay = (RelativeLayout) findViewById(R.id.back_back);
    		lay.setBackgroundResource(R.drawable.main_background);
    		ImageView iobstacle = (ImageView) this.findViewById(R.id.obstacle_bus);
    		iobstacle.setVisibility(View.INVISIBLE);
    		iPipe.setVisibility(View.VISIBLE);
    		map = 2;
    		playMusic(4); 
        } else {
        	
        	playMusic(5); 
        }
        hpTv = (TextView) findViewById(R.id.hp_text);
        hpTv.setText("HP: "+ge.getPlayerHp()+"/"+ge.getPlayerMaxHp());
        ammoTv = (TextView) findViewById(R.id.ammo_text);
        ammoTv.setText("Ammo: "+ge.getAmmo()+"/"+ge.getMaxAmmo());
        
        glock9mmV = (ImageView) findViewById(R.id.glock9mm);       
		
        glock9mmV.setAlpha(0.8f);        
		glock9mmV.setOnClickListener(listener);
		glock9mmV.setY(glock9mmV.getY()-50);
		
        shotgunV = (ImageView) findViewById(R.id.shotgun);       
        shotgunV.setMaxWidth(25);   	
        shotgunV.setMaxHeight(25);		
		shotgunV.setAlpha(0.8f);        
		shotgunV.setOnClickListener(listener);
		
        molotovV = (ImageView) findViewById(R.id.molotov);       
        molotovV.setMaxWidth(25);   	
        molotovV.setMaxHeight(25);		
        molotovV.setAlpha(0.8f);        
        molotovV.setOnClickListener(listener);		

//		No obstacles in current iteration of the ZDawn, saveing code for google play edition
//	    ImageView iobstacle = (ImageView) this.findViewById(R.id.obstacle_Image);
		RelativeLayout lay = (RelativeLayout) this.findViewById(R.id.back);
		lay.setOnClickListener(listener); // Need to listen to all shots that miss too!
		lay.setOnTouchListener(touchy); // Add in ricochet graphics
//		lay.addView(iobstacle); is XML, don't need to add it again
//		lay.removeView(iobstacle); // Lets get rid of this for now
 	       

        //Get rid of ActionBar
        ActionBar  actionBar = getActionBar();
        actionBar.hide();
        //Get rid of notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        

        getScreen();
    }
    
	private OnClickListener listener = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			Log.d("GPActivity","Listener called from:"+v.getId());
						
			switch (v.getId()) {
				case R.id.glock9mm: 
					if(!ge.isReloading()) { // need to check ammo is not already full, dont have to reload
						if(ge.getWeaponType() == 1) {	
							if(ge.getAmmo() != ge.getMaxAmmo()) {
								ge.reload();
								setReloadText();
								Log.d("GPActivity","Reloading glock, ID:"+v.getId());
							}	
						} else {
							ge.swapWeapon();
							updateAmmoTv();
							if(!isGlock) {
								isGlock = true;
							glock9mmV.setY(glock9mmV.getY()-50);
							shotgunV.setY(shotgunV.getY()+50);
							}
						}
					}
				break;				
				
				case R.id.shotgun: 
					Log.d("GPActivity","Shotgun chosen, ID:"+v.getId());
					if(!ge.isReloading()) {
						if(ge.getWeaponType() == 2) {
							if(ge.getAmmo() != ge.getMaxAmmo()) {
								ge.reload();
								setReloadText();
								Log.d("GPActivity","Shotgun reloading, ID:"+v.getId());
							}
						} else {
							ge.swapWeapon();
							updateAmmoTv();
							if(isGlock) {
								isGlock = false;
								shotgunV.setY(shotgunV.getY()-50);
								glock9mmV.setY(glock9mmV.getY()+50);
							}
							
						}
					}
				break;				
				
				case R.id.molotov: 
					if(!ge.isReloading()) {
						v.setAlpha(1f);
						throwMolotov();
						
						SoundHandler.getFx(GamePlayActivity.this, "lighter");

						Log.d("GPActivity","Throwing molotov, ID:"+v.getId());
						}
				break;
				
				default:
					Log.d("GPActivity","miss");
					
	    		   if(!ge.isReloading()) {
	    			   if(ge.getAmmo() < 1) {
	    				   setReloadText();
	    				   ge.reload();
	    			   } else {
	    				   playWeaponFX(false);
	    				   ge.missShot();
	    				   updateAmmoTv();
	    			   }  
	    		   } 
				break;				
			} 
				
		}		
	};
    
	private OnTouchListener touchy = new OnTouchListener() {		
		@Override
		  public boolean onTouch(View v, MotionEvent event) {
			
			int touchPositionX = (int)event.getX();
		    int touchPositionY = (int)event.getY();

		    final ImageView irico = (ImageView) findViewById(R.id.ricochet);
		    irico.setVisibility(1);

		    irico.setX(touchPositionX);
		    irico.setY(touchPositionY);
		    
		    Log.d("GP","Ricochet X/Y: "+touchPositionX+"/"+touchPositionY);
			
			Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
				public void run() {	  					
					irico.setVisibility(View.GONE);	
					Log.d("GP","Ricochet Gone");
				}
			}, 500);
				    	
			return false;
		}

	};
	
	private void throwMolotov() {
        final ImageView molotov = (ImageView) findViewById(R.id.molotov);
		final ViewPropertyAnimator animator = molotov.animate();
		animator.scaleX(5);
		animator.scaleY(5);
		animator.setDuration(2000);
		animator.x(1000).y(100);

		 Runnable endAction = new Runnable() {
			 Runnable endAction2 = new Runnable() {
				 Runnable endAction3 = new Runnable() {
				     public void run() {				    	 
				         molotov.setVisibility(6);
				    	 molotov.setImageResource(R.drawable.molotov);
				         SoundHandler.getFx(GamePlayActivity.this, "burn");				         

				         Log.d("GPActivity","3 - Molotov Explosion");
				     }
				 };
			     public void run() {
			    	 molotov.setImageResource(R.drawable.explosion);
			         animator.setDuration(900).scaleX(8).scaleY(8).withEndAction(endAction3);
			         ge.throwMolotov();
					 SoundHandler.getFx(GamePlayActivity.this, "breakglass");
			         
			         Log.d("GPActivity","2 - Molotov Thrown");
			     }
			 };
		     public void run() {
		         animator.setDuration(900).x(600).y(500).scaleX(1).scaleY(1).withEndAction(endAction2);
		         SoundHandler.getFx(GamePlayActivity.this, "throw");		         

		         Log.d("GPActivity","1 - Throwing Molotov");
		     }
		 };

		animator.withEndAction(endAction);
		animator.start();	
	}
    // @David
	@Override
    public void onBackPressed() {  
    	//Stops and releases the game music on back button
		super.onBackPressed();
    	SoundHandler.onBackStopSound();
    	ge.stopTimer();
    	finish();
    	try {
			SoundHandler.getMusic(this, 2);
		} catch (IOException e) {
			e.printStackTrace(); 
		}
    }
	
    
	/*
	 * Plays when shots fired. Also called by ZView, in which case arg is true
	 */
	public void playWeaponFX(boolean isZombieHit) {

		if(ge.getWeaponType() == 1) SoundHandler.getFx(this, "pistol");
		
		else SoundHandler.getFx(this, "shotgun");
		
		if(isZombieHit) SoundHandler.getMoan(this);

	}
    
    @Override
    public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName().equals("reloading")) {
			Log.d("markus", "property change game play " +event.getNewValue());
			runOnUiThread(new Runnable() {
				public void run() {
					updateAmmoTv();
				}
			}); 
		}
		if(event.getPropertyName().equals("spawnZombie")) {
			runOnUiThread(new Runnable() {
				public void run() {
					Log.d("markus", "gameplay spawn zombie event");
					new ZView(GamePlayActivity.this, ge.createZombie());
				}
			});
			
		}
		if(event.getPropertyName().equals("playerDead")) {
			runOnUiThread(new Runnable() {
				public void run() {
					Log.d("GamePlayActivity","Player Dead");
					playerDeadAni();
					ge.killAllZombies();
					new Timer().schedule(new TimerTask() {
						@Override
						public void run() {
							Intent intent = new Intent(GamePlayActivity.this, SafeHouseActivity.class);
							intent.putExtra("completedLevel", ge.getLevel());
							intent.putExtra("isDead", true);
							startActivity(intent);
							ge.stopTimer();
							finish();
						}
					}, 2000);
				}
			});
		}
		if(event.getPropertyName().equals("playerAttacked")) {
			runOnUiThread(new Runnable() {
				public void run() {
					Log.d("ZView","Player Attacked");
					playerAttackedAni();
					if(ge.getPlayerHp() > 0) {
						hpTv.setText("HP: "+ge.getPlayerHp()+"/"+ge.getPlayerMaxHp());
					} else {
						hpTv.setText("HP: 0"+"/"+ge.getPlayerMaxHp());
					}
					
				}
			});
		}
		if(event.getPropertyName().equals("levelComplete")) {
			SharedPreferences.Editor spEdit = sp.edit();
			spEdit.putInt("level", ge.getLevel()+1).commit();
			Intent intent = new Intent(this, SafeHouseActivity.class);
			intent.putExtra("zombieKillScore", ge.getZombieKillScore());
			intent.putExtra("molotovKillScore", ge.getMolotovKillScore());
			intent.putExtra("completedLevel", ge.getLevel());
			
			ge.stopTimer();

			startActivity(intent);

			finish();
		}
    }

	private void playerDeadAni() {
		ImageView playerDead = (ImageView) findViewById(R.id.bleed_background);       		
		playerDead.setAlpha(0.8f); 
		playerDead.setVisibility(View.VISIBLE);
		
		playerDead.bringToFront();
		
		TextView gameOverTv = (TextView) findViewById(R.id.game_over);
		gameOverTv.setVisibility(1);
		gameOverTv.bringToFront();
		SoundHandler.getFx(this, "eating");
		Log.d("GPAct","PlayerDeadAni called");
		
	}
	
	/*
	 * Called by ZView when a zombie dies, arg is the zombie view to be removed
	 */
	public void rmZombie(View v) {
      
		RelativeLayout foobar = (RelativeLayout) findViewById(R.id.back);	
		foobar.removeView(v);

	}
	
	/*
	 * Called by ZView when a zombie dies
	 */
	public void dieZombieSound() {
	      
		SoundHandler.getFx(this, "zombiedie");


	}

	private void playerAttackedAni() {
		
		if(ge.getPlayerHp() > 1) {
		
		ImageView iBleed = (ImageView) findViewById(R.id.bleed_background);       

		iBleed.setAlpha(0.5f);
		iBleed.bringToFront();
		
		Animation anim = new AlphaAnimation(1.0f, 0.0f);
		
		anim.setDuration(300); 
		anim.setStartOffset(500);
		anim.setRepeatMode(Animation.REVERSE);
		anim.setRepeatCount(Animation.INFINITE);
		iBleed.startAnimation(anim);
	
		Log.d("GPAct","PlayerAttackedAni called");
		}				
	}
	
	/*
	 * Called by ZView
	 */
	public void updateAmmoTv() {
		ammoTv.setText("Ammo: "+ge.getAmmo()+"/"+ge.getMaxAmmo());
	}
	/*
	 * Called by ZView
	 */
	public void setReloadText() {
		ammoTv.setText("reloading");
	}
	
	@Override
    public void onStop() {
     super.onStop();
     Log.d("GP", "onStop called");
 //    ge.stopTimer();
	     ge.killAllZombies();

     
	}
	
	private void playMusic(int track) {
		try {
			SoundHandler.getMusic(this, track);
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	public void rearrangeZ(AbstractZombie z) {
		int indexPos = 1;
		
		RelativeLayout lay = (RelativeLayout) this.findViewById(R.id.back);
		
//		ge.getPositionInList(z);
		if(map == 1) {
		ImageView iobstacle = (ImageView) this.findViewById(R.id.obstacle_bus);
		lay.removeView(iobstacle);
		lay.addView(iobstacle,indexPos);
		Log.d("GP","Putting obstacle in Zindex:"+indexPos);
		}
		
		if(map == 2) {
		ImageView iobstacle = (ImageView) this.findViewById(R.id.obstacle_pipe);
		lay.removeView(iobstacle);
		lay.addView(iobstacle,indexPos);
		Log.d("GP","Putting obstacle in Zindex:"+indexPos);
		}		
	}
	
	public boolean getScreen() {
		
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		int height = size.y;
		int centerX = width / 2;
		int centerY = height / 2;
		
		Log.d("GPA", "Screen size: x:"+size.x+" y:"+size.y);
		Log.d("GPA", "Center: x:"+centerX+" y:"+centerY);
	
//	    final ImageView img	= new ImageView(this);
		
//		final TextView img = new TextView(this);
		
//		img.setText("Center: X:"+centerX+" Y: "+centerY);
		
	    final ImageView img = (ImageView) findViewById(R.id.ricochet); //dont need to addView
	    img.setImageResource(R.drawable.splatter1);
//	    img.setVisibility(1);
//		img.setAdjustViewBounds(true); 		// must have to use setMaxWidth/Height
		img.setMaxWidth(180); 				// shrink a hi rez picture   	
		img.setMaxHeight(180);
	    
	    img.setX(centerX);
	    img.setY(centerY);
	    
		img.setAlpha(0.9f);
		img.bringToFront();
		
		Animation anim = new AlphaAnimation(1.0f, 0.0f);
		
		anim.setDuration(300); 
		anim.setStartOffset(500);
		anim.setRepeatMode(Animation.REVERSE);
		anim.setRepeatCount(Animation.INFINITE);
		img.startAnimation(anim);
	    		
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			public void run() {	  					
				img.setVisibility(View.GONE);	
			}
		}, 6000);
	
		return true;
	
	}
}
