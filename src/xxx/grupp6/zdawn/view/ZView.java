package xxx.grupp6.zdawn.view;

import java.beans.PropertyChangeEvent;


import java.beans.PropertyChangeListener;
import java.util.Random;

import xxx.grupp6.zdawn.R;
import xxx.grupp6.zdawn.model.GameEngine;
import xxx.grupp6.zdawn.model.PuppetMaster;
import xxx.grupp6.zdawn.model.SoundHandler;
import xxx.grupp6.zdawn.model.zombies.AbstractZombie;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.ViewPropertyAnimator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * @author Tilo / Grupp 6, ITHS
 * 2014-12-02
 * All zombies generated from here
 *
 */


public class ZView implements PropertyChangeListener{
	private ImageView img;
	
	private GameEngine ge;	
	private AbstractZombie z;
	
	private GamePlayActivity gpAct;
	
	private int zSkin = 0;
	
    final PuppetMaster pm;

	public ZView(final GamePlayActivity gpAct, final AbstractZombie z) {
				
		this.z = z;
		this.gpAct = gpAct;
		
		pm	= new PuppetMaster();
		img	= new ImageView(gpAct);
		ge	= GameEngine.getInstance();
		
		ge.addPropertyChangeListener(this);
	        
        zSkin = new Random().nextInt(3)+1;
        setImg(z);
        		
		img.setAdjustViewBounds(true); 		// must have to use setMaxWidth/Height
		img.setMaxWidth(180); 				// shrink a hi rez picture   	
		img.setMaxHeight(180);
//		img.setVisibility(View.VISIBLE);
		img.setX(pm.getStartPosX());		// Ask puppetmaster where am I?
		img.setY(pm.getStartPosY());
		img.setOnClickListener(listener);	// Shoot 'em
		img.setOnTouchListener(touchy);		// Testing Headshot

		
		Animation anim = new AlphaAnimation(0.0f, 1.0f); // Simple fade in animation
		anim.setDuration(1000);		
		img.setAnimation(anim);
	
		final RelativeLayout lay = (RelativeLayout) gpAct.findViewById(R.id.back);
		lay.addView(img,0); // Whats the second arg for?
		
		final ViewPropertyAnimator animator = img.animate();	// The magical View
		animator.scaleX(2);			// Zoom
		animator.scaleY(2);
		animator.setDuration(8000);
		animator.x(pm.getMidPosX()).y(pm.getMidPosY());			// The walking dead
		
		 Runnable endAction = new Runnable() {		// First chain
			 Runnable endAction2 = new Runnable() {	// Second chain
			     public void run() {
			    	 Log.d("ZView","Trying to Eat Braaiiiins!");
			    	 
//			    	 lay.addView(img,2);
//			    	 Log.d("ZView","index: "+lay.indexOfChild(img));
			    	 if(!ge.isDead(z)) {
			    		 ge.startPlayerDmgTimer(z);	
			    	 } 
			     }
			 };
		     public void run() {
		    	 gpAct.rearrangeZ(z);
		    	 

		         animator.x(pm.getEndPosX()).y(pm.getEndPosY()).scaleX(15).scaleY(15);
		         animator.withEndAction(endAction2);		         
		     }
		 };

		animator.withEndAction(endAction);	// Chain it
		animator.start();
		
//		sendViewToBack(img);
	}
	
	// Show animation for zombie hit
	private void addBlood(AbstractZombie z2) {
		if(z2.getType() == 1) {

			switch (zSkin) {
			case 1:  img.setImageResource(R.drawable.zombie1_hit); break;
			case 2:	 img.setImageResource(R.drawable.zombie2_hit); break;
			case 3:	 img.setImageResource(R.drawable.zombie3_hit); break;
			default: img.setImageResource(R.drawable.zombie1_hit); break;
			}
			
		}
		if(z2.getType() == 2) {
			img.setImageResource(R.drawable.zombie4_hit); 
		}

			Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
					public void run() {	       						
						setImg(z); }
				}, 50);
	}
	
	// Match the zombie picture with its type, random it type 1, and remember it
	protected void setImg(AbstractZombie z2) {
			if(z2.getType() == 1) {

												
				switch (zSkin) {
				case 1:  img.setImageResource(R.drawable.zombie1); zSkin = 1;	break;
				case 2:	 img.setImageResource(R.drawable.zombie2); zSkin = 2;	break;
				case 3:	 img.setImageResource(R.drawable.zombie3); zSkin = 3;	break;
				default: img.setImageResource(R.drawable.zombie1); zSkin = 1;	break;
				}				
			}
			if(z2.getType() == 2) {
				img.setImageResource(R.drawable.zombie4); zSkin = 4;
			}		
	}

// Not using ontouch
	private OnTouchListener touchy = new OnTouchListener() {		
		@Override
		  public boolean onTouch(View v, MotionEvent event) {
			
			int touchPositionX = (int)event.getX();
		    int touchPositionY = (int)event.getY();
		    Log.d("ZView onTouch","X:"+touchPositionX);
		    Log.d("ZView onTouch","Y:"+touchPositionY);			
	    	
			return false;
		}

	};
	
	private OnClickListener listener = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			
			Log.d("ZView","Onclick X co ordinate:"+v.getX());
			Log.d("ZView","Onclick Y co ordinate:"+v.getY());
			
			if(!ge.isReloading()) {
	    		   if(ge.getAmmo() < 1) {
	    			   gpAct.setReloadText();
	    			   ge.reload();
	    		   } else {   
	        	   ge.takeDamage(z);
	        //	   hpTv.setText(ge.getHp(z)+"/10");
	        	   gpAct.updateAmmoTv();
	        	   gpAct.playWeaponFX(true);	        	   

	        	   SoundHandler.getMoan(gpAct);

	        	   addBlood(z);
	    		   } 
			}
	    	   
		}

	};

	
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if(event.getPropertyName().equals("zombieDead") && z.isDead()) {
			gpAct.dieZombieSound();

			Animation anim = new AlphaAnimation(1.0f, 0.0f);
			anim.setDuration(700);		
			img.setAnimation(anim);
			anim.start();			
			
			Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
				public void run() {	
					img.clearAnimation();
//					img.setVisibility(0);
					gpAct.rmZombie(img);
					Log.d("GP","Zombie calling GP to be removed");
				}
			}, 700);
			
		}
		if(event.getPropertyName().equals("killAll")) {
			img.clearAnimation();
			gpAct.rmZombie(img);
		}
		
	}
/*
	public static void sendViewToBack(final View child) {
	    final ViewGroup parent = (ViewGroup)child.getParent();
	    if (null != parent) {
	        parent.removeView(child);
	        parent.addView(child, 0);
	        
	    }
	}
	*/
}

/*
public void onPropertyChange(PropertyChangeEvent e) {
  if (e.getName().equals("takeDamage")): change image into zombieDamage.png. Start timer which changes it back in 0.5s
  if (e.getName().equals("move")): change image x,y coordinates
}

}

public class Zombie {
public takeDamage() {
hp--;
fire event "takeDamage" by property change
}
}

*/
