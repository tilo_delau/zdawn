package xxx.grupp6.zdawn.view;

import java.io.IOException;

import xxx.grupp6.zdawn.R;
import xxx.grupp6.zdawn.model.SoundHandler;
import xxx.grupp6.zdawn.view.memberdata.CameraHandler;
import xxx.grupp6.zdawn.view.memberdata.DBHelper;
import xxx.grupp6.zdawn.view.memberdata.PlayerInfo;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author David Calderon
 * 
 * Activity for user options 
 * where the user can change names, 
 * take apicture and add it to their highscore list,
 * see the highscore,
 * turn music on/off, turn location on/off.
 * 
 * Uses SharedPreferences 
 */
public class GameOptionsActivity extends Activity {

	private PlayerInfo pi;
	private EditText changeName;
	private Button setName;
	private String name;
	private TextView text;
	private Switch musicSwitch;
	private Switch locationSwitch;

	
	
	@SuppressLint("CutPasteId")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_options);
		removeActionBar();

		pi = new PlayerInfo(this);
		new DBHelper(this);

		changeName = (EditText) findViewById(R.id.edit_name);
		changeName.setVisibility(View.GONE);

		text = (TextView) findViewById(R.id.playing_as);
		text.setText("Playing as: " + pi.getName());

		musicSwitch = (Switch) findViewById(R.id.music_switch);
		locationSwitch = (Switch) findViewById(R.id.location_switch);

		setName = (Button) findViewById(R.id.ok_button);
		setName.setOnClickListener(buttonListener);
		setName.setVisibility(View.GONE);

		musicSwitch = (Switch) findViewById(R.id.music_switch);
		locationSwitch = (Switch) findViewById(R.id.location_switch);

		SharedPreferences sh = this.getSharedPreferences("xxx.grupp6.zdawn.SETTING", Context.MODE_PRIVATE);
		locationSwitch.setChecked(sh.getBoolean("LocationSwitchState", true));;
		musicSwitch.setChecked(sh.getBoolean("MusicSwitchState", true));;

		Button button = (Button) findViewById(R.id.return_from_options_button);
		button.setOnClickListener(buttonListener);
		button = (Button) findViewById(R.id.return_button);
		button.setOnClickListener(buttonListener);
		button = (Button) findViewById(R.id.location_switch);
		button.setOnClickListener(buttonListener);
		button = (Button) findViewById(R.id.music_switch);
		button.setOnClickListener(buttonListener);
		button = (Button) findViewById(R.id.change_name_button);
		button.setOnClickListener(buttonListener);
		Button cameraButton = (Button) findViewById(R.id.camera_button);
		cameraButton.setOnClickListener(buttonListener);
		button = (Button) findViewById(R.id.highscore_button);
		button.setOnClickListener(buttonListener);
	}

	private void menuOptions(View v) {
		switch (v.getId()) {
		case R.id.return_button: {
			Intent intent = new Intent(this, MainMenuActivity.class);
			startActivity(intent);
			break;
		}
		case R.id.location_switch: {
			if(locationSwitch.isChecked()) {
				SharedPreferences sh = this.getSharedPreferences("xxx.grupp6.zdawn.SETTING", Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = sh.edit();
				editor.putBoolean("showLocation", true);
				editor.putBoolean("LocationSwitchState", true);
				editor.commit();

				Toast.makeText(getApplicationContext(), "Your location is: " + pi.getLocation(),
						Toast.LENGTH_SHORT).show();
			}
			else {
				SharedPreferences sh = this.getSharedPreferences("xxx.grupp6.zdawn.SETTING", Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = sh.edit();
				editor.putBoolean("showLocation", false);
				editor.putBoolean("LocationSwitchState", false);
				editor.commit();

				Toast.makeText(getApplicationContext(), "Location has been turned off",
						Toast.LENGTH_SHORT).show();

				Log.e("SWITCH","LOCATION is currently OFF");
			}
			break;
		}

		case R.id.music_switch: {

			if(musicSwitch.isChecked()) {
				playMusic(2);
				SharedPreferences sh = this.getSharedPreferences("xxx.grupp6.zdawn.SETTING", Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = sh.edit();
				editor.putBoolean("MusicSwitchState", true);
				editor.commit();
 
			} else {  
				SoundHandler.mp.stop();
				SharedPreferences sh = this.getSharedPreferences("xxx.grupp6.zdawn.SETTING", Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = sh.edit();
				editor.putBoolean("MusicSwitchState", false);
				editor.commit();
			} 
			break;
		}

		case R.id.ok_button: {
			name = ((EditText) findViewById(R.id.edit_name)).getText()
					.toString();
			pi.changeName(name);
			text.setText("Player: "+ name);
			Toast.makeText(getApplicationContext(), "Changed name to: " + name,
					Toast.LENGTH_LONG).show();
			setName.setVisibility(View.GONE);
			changeName.setVisibility(View.GONE);
			break;
		}

		case R.id.change_name_button: {
			changeName.setVisibility(View.VISIBLE);
			setName.setVisibility(View.VISIBLE);
			break;
		}
		case R.id.camera_button: {
			Intent intent = new Intent(this, CameraHandler.class);
			startActivity(intent);
			break;
		}
		case R.id.highscore_button: {
			Intent intent = new Intent(this, HighscoreActivity.class);
			startActivity(intent);
			break;
		}
		}	
	}

	private void playMusic(int track) {
		try {
			SoundHandler.getMusic(this, track);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void removeActionBar() {
		//Get rid of ActionBar
		ActionBar  actionBar = getActionBar();
		actionBar.hide();
		//Get rid of notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu
		getMenuInflater().inflate(R.menu.game_options, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private OnClickListener buttonListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			menuOptions(v);
		}
	};

}
