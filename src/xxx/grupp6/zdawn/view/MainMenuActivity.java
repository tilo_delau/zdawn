package xxx.grupp6.zdawn.view;

import java.io.IOException;

import xxx.grupp6.zdawn.R;
import xxx.grupp6.zdawn.model.SoundHandler;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import xxx.grupp6.zdawn.view.memberdata.CameraHandler;
import xxx.grupp6.zdawn.view.memberdata.LocationHandler;
import xxx.grupp6.zdawn.view.memberdata.PlayerInfo;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;


/**
 * 
 * @author it-hogskolan, Grupp 6, David,Daniel,Tilo,Markus,2014
 * Our start menu, choose among many magical buttons
 *
 */
public class MainMenuActivity extends Activity {

	private SharedPreferences sp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_menu);
		removeActionBar();
		Button newGameButton = (Button) findViewById(R.id.new_game_button);
		newGameButton.setOnClickListener(buttonListener);

		Button aboutButton = (Button) findViewById(R.id.about_button);
		aboutButton.setOnClickListener(buttonListener);

		Button optionsButton = (Button) findViewById(R.id.options_button);
		optionsButton.setOnClickListener(buttonListener);

		Button musicButton = (Button) findViewById(R.id.music_button);
		musicButton.setOnClickListener(buttonListener);

		sp = getSharedPreferences("xxx.grupp6.zdawn.SETTING",
				Context.MODE_PRIVATE);

		LocationHandler.getInstance(this);

		if (SoundHandler.onTrack || !SoundHandler.switchedOff) {
			Log.v("Main", "playing music on create");
			// if (SoundHandler.onTrack) {
			try {
				SoundHandler.getMusic(this, 2);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		setMuteBtn();
	}

	/**
	 * Checks if player exists or not and draw correct button depending on
	 * checkresult Not done yet. Need tweaking if we'll keep it.
	 */
	private void continueButton() {
		if (sp.getLong("playerId", -1) <= 0) {
			Log.d("datta", "if sp -1." + sp.getLong("playerId", -1));
			Button loadGameButton = (Button) findViewById(R.id.load_button);
			loadGameButton.setText("Can't continue..");
		} else if (sp.getLong("playerId", -1) >= 0) {
			Button loadGameButton = (Button) findViewById(R.id.load_button);
			loadGameButton.setOnClickListener(buttonListener);
			loadGameButton.setText("Continue...");
			loadGameButton.setVisibility(View.VISIBLE);
			Log.d("datta", "if sp 0+." + sp.getLong("playerId", -1));
		}
	}

	private void removeActionBar() {
		// Get rid of ActionBar
		ActionBar actionBar = getActionBar();
		actionBar.hide();
		// Get rid of notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}

	@Override
	protected void onStart() {
		super.onStart();
		continueButton();
		setMuteBtn();
	}

	protected void onPause() {
		super.onPause();

		if (SoundHandler.mp != null && SoundHandler.mp.isPlaying()) {
			SoundHandler.mp.stop();
		} else if (SoundHandler.switchedOff) {
			return;
		}
		setMuteBtn();
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (!SoundHandler.switchedOff) {
			try {
				SoundHandler.getMusic(this, 2);

			} catch (IOException e) {
				e.printStackTrace();
			}
			setMuteBtn();
		}

	}

	// Stops and releases music
	@Override
	protected void onStop() {
		super.onStop();

		// Stops and releases the game music
		if (SoundHandler.mp != null && SoundHandler.mp.isPlaying()) {
			SoundHandler.mp.stop();
			// insert condition
		} else if (SoundHandler.switchedOff) {
			return;
		}
		// SoundHandler.stopSound();
	}

	private void menuOptions(View v) {
		switch (v.getId()) {
		case R.id.new_game_button: {
			PlayerInfo pi = new PlayerInfo(this.getBaseContext());
			pi.resetScore();
			SharedPreferences.Editor spEdit = sp.edit();
			spEdit.putInt("level", 1).commit();
			Intent intent = new Intent(this, IntroActivity.class);
			startActivity(intent);
			break;
		}
		case R.id.load_button: {
			Intent intent = new Intent(this, SafeHouseActivity.class);
			startActivity(intent);
			playMusic(3);
			break;
		}
		case R.id.about_button: {
			Intent intent = new Intent(this, AboutUsActivity.class);
			startActivity(intent);
			break;
		}
		case R.id.options_button: {
			Intent intent = new Intent(this, GameOptionsActivity.class);
			startActivity(intent);
			break;
		}

		case R.id.camera_button: {
			Intent intent = new Intent(this, CameraHandler.class);
			startActivity(intent);
			break;
		}
		case R.id.music_button: {
			Log.d("MenuActivity", "Music button pressed");
			toggleMuteBtn();

		}
			break;

		}
	}

	private void playMusic(int track) {
		if (isMusicSwitchOn()) {
			try {
				SoundHandler.getMusic(this, track);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	private void toggleMuteBtn() {
		Button muteBtn = (Button) findViewById(R.id.music_button);

		if (SoundHandler.mp != null && SoundHandler.mp.isPlaying()) {
			muteBtn.setBackgroundResource(R.drawable.music_button_mute);
			SoundHandler.mp.stop();
		} else {
			muteBtn.setBackgroundResource(R.drawable.music_button_on);
			playMusic(2);
		}
		setMuteBtn();

	}

	private void setMuteBtn() {
		Button muteBtn = (Button) findViewById(R.id.music_button);

		if (SoundHandler.mp != null && SoundHandler.mp.isPlaying()) {
			muteBtn.setBackgroundResource(R.drawable.music_button_on);
		} else {
			muteBtn.setBackgroundResource(R.drawable.music_button_mute);
		}		
		
	}

	private boolean isMusicSwitchOn() {
		boolean musicSwitch;
		SharedPreferences sh = this.getSharedPreferences(
				"xxx.grupp6.zdawn.SETTING", Context.MODE_PRIVATE);
		musicSwitch = sh.getBoolean("MusicSwitchState", true);
		return musicSwitch;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private OnClickListener buttonListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			menuOptions(v);
		}
	};

}
