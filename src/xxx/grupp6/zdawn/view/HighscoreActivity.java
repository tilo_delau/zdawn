package xxx.grupp6.zdawn.view;

import xxx.grupp6.zdawn.R;
import xxx.grupp6.zdawn.view.memberdata.DBHelper;
import xxx.grupp6.zdawn.view.memberdata.HighscoreAdapter;
import android.app.ActionBar;
import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

/**
 * 
 * @author daniel - Grupp 6
 * 
 * Show highscore as a ListView and obtain data from SQLite.
 *
 */
public class HighscoreActivity extends Activity {
	private DBHelper dbh;
	HighscoreAdapter adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_highscore);
		removeActionBar();
		
		Button button = (Button)findViewById(R.id.highscore_magic_button);
		button.setOnClickListener(buttonListener);
		dbh = new DBHelper(this);
		ListView list = (ListView) findViewById(R.id.score_list);
		Cursor showDB = dbh.showToAdapter();
		adapter = new HighscoreAdapter(this, showDB);
		list.setAdapter(adapter);
		
		
		
	}

	private void removeActionBar() {
        //Get rid of ActionBar
        ActionBar  actionBar = getActionBar();
        actionBar.hide();
        //Get rid of notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}
	
	private void makePlayers(){
			dbh.createFalsePlayer("Arnold S", "California", 22);
			dbh.createFalsePlayer("Stallone", "Miami", 18);
			dbh.createFalsePlayer("C.Norris", "Saigon", 100);
			dbh.createFalsePlayer("VanDamme", "Hollywood", 21);
			dbh.createFalsePlayer("Kungen", "Stockholm", 99);
			dbh.createFalsePlayer("UffeL", "Dimman", 3);
			dbh.createFalsePlayer("Glenn", "Göteborg", 50);
			finish();
			startActivity(getIntent());
		
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.highscore, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private OnClickListener buttonListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			makePlayers();
		}
	};
}
