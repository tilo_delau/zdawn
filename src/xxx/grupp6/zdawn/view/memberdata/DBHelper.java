package xxx.grupp6.zdawn.view.memberdata;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * 
 * @author Daniel - Grupp 6
 * 
 *         SQLite Helper. Creating ZDawns database, altering database structure
 *         and handling CRUD commands.
 * 
 */
public class DBHelper extends SQLiteOpenHelper {
	private ContentValues row = new ContentValues();
	private SQLiteDatabase db;
	private SharedPreferences sh;
	private Context context;

	// Database Version
	private static final int DB_VERSION = 3;
	// Databse structure
	private static long playerId;
	private static String playerName;
	private static final String TABLE_HIGHSCORE = "Highscore";
	private static final String COLUMN_ID = "_id";
	private static final String COLUMN_NAME = "name";
	private static final String COLUMN_LOCATION = "location";
	private static final String COLUMN_SCORE = "score";
	private static final String COLUMN_PHOTO = "photo";
	private static final String COLUMN_GAMELEVEL = "gamelevel";
	private static final String CREATE_HIGHSCORE = "CREATE TABLE "
			+ TABLE_HIGHSCORE + "(" + COLUMN_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_NAME
			+ " VARCHAR(20) NOT NULL, " + COLUMN_LOCATION + " VARCHAR(20), "
			+ COLUMN_SCORE + " INTEGER, " + COLUMN_PHOTO + " VARCHAR(250), "
			+ COLUMN_GAMELEVEL + " INTEGER);";

	// Delete databse
	private static final String DROP_HIGHSCORE = "DROP TABLE IF EXISTS "
			+ TABLE_HIGHSCORE + ";";

	/**
	 * Constructor saving the Context from the Activity calling it.
	 * Context is used to alter SharedPreferences.
	 * 
	 * @param Context context
	 */
	public DBHelper(Context context) {
		super(context, "HighscoreDB", null, DB_VERSION);
		this.context = context;
		setPlayerId();
		Log.d("DBHelper", "DBHelper: constructor done. PlayerId: "+playerId);

	}
	/**
	 * Setting playerID to value stored in shared preferences.
	 */
	private void setPlayerId() {
		sh = context.getSharedPreferences("xxx.grupp6.zdawn.SETTING",
				Context.MODE_PRIVATE);
		playerId = sh.getLong("playerId", 666);
	}

	/**
	 * Method to save players unique id and name from SQLite to sharedpreferences.
	 * 
	 * @param playerId
	 */
	private void sqlToSharedPref() {
		sh = context.getSharedPreferences("xxx.grupp6.zdawn.SETTING",
				Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sh.edit();
		editor.putLong("playerId", playerId);
		editor.putString("playerName", playerName);
		editor.commit();
		Log.d("DBHelper", "playerId commited to sharedPref");
	}
	/**
	 * Create the SQL database.
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_HIGHSCORE);
		Log.d("DBHelper", "sql created");
	}
	/**
	 * When upgrading tow SQL structure we first delete the old one before
	 * creating the new version.
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(DROP_HIGHSCORE);
		onCreate(db);
	}

	/**
	 * Returning a cursor to cursorAdapter with highscore database data.
	 * 
	 * @return Cursor database
	 */
	public Cursor showToAdapter() {
		String orderBy = "score DESC";
		return getReadableDatabase().query(TABLE_HIGHSCORE, null, null, null,
				null, null, orderBy);

	}

	/*
	 * For debugging purpouses. Only used by devs.
	 * Shows the whole database in Logs.
	 */
	public void showDB() {
		Log.d("DBHelper", "Entered showDB()");
		db = getReadableDatabase();
		Log.d("DBHelper", "got ReadableDatabase");
		Cursor c = db
				.query(TABLE_HIGHSCORE, null, null, null, null, null, null);
		if (c.moveToFirst()) {
			do {
				Log.d("DBHelper",
						"Show the wole DB: \n_id: " + c.getInt(0) + ". name: "
								+ c.getString(1) + ". Location: "
								+ c.getString(2) + ". Points: " + c.getInt(3)
								+ ". Photo: " + c.getString(4)
								+ " Variable playerId " + playerId
								+ " GameLEvel " + c.getInt(5));
			} while (c.moveToNext());
		}
		db.close();
		sh = context.getSharedPreferences("xxx.grupp6.zdawn.SETTING",
				Context.MODE_PRIVATE);

		// getting value fromn SharedPreferences
		long test = sh.getLong("playerId", -1);
		Log.d("DBHelper", "playerId from SH: " + test);

	}

	/**
	 * When register a new player, player is given a random zombie-name.
	 * As a result of the insert we will obtain a long-value. That long
	 * will be set as the player unique ID. It's saved in sharedPreferenses
	 * along with the playername.
	 * 
	 * @param String name
	 */
	public void createNewPlayerDB(String name) {
		playerName = name;
		db = getWritableDatabase();
		row.put("name", name);
		playerId = db.insert(TABLE_HIGHSCORE, null, row);
		Log.d("DBHelper", "CreateNewPlayerDB playerId: " + playerId);
		db.close();
		sqlToSharedPref();

	}
	/**
	 * This is for demo-only. Not the live-version. This metod creates a
	 *  false player to show in highscore.
	 * 
	 * @param String name
	 * @param String city
	 * @param int score
	 */
	public void createFalsePlayer(String name, String city, int score) {
		db = getWritableDatabase();
		row.put("name", name);
		row.put("location", city);
		row.put("score", ""+score);
		db.insert(TABLE_HIGHSCORE, null, row);
		Log.d("DBHelper", "CreateNewPlayerDB playerId: ");
		db.close();
	}
	
	/**
	 * This changes the players name. It updates SQL, the classvariable and sharedpreferences.
	 * 
	 * @param String newName
	 */
	public void changeName(String newName){
		db = getWritableDatabase();
		row.put("name", newName);
		String[] whereArgs = { Long.toString(playerId) };
		db.update(TABLE_HIGHSCORE, row, COLUMN_ID + "=?", whereArgs);
		db.close();
		playerName = newName;
		sqlToSharedPref();
	}
	/**
	 * Method to add a players location.
	 * 
	 * @param String location
	 */
	public void addLocation(String loc) {
		db = getWritableDatabase();
		row.put("location", loc);
		String[] whereArgs = { Long.toString(playerId) };
		db.update(TABLE_HIGHSCORE, row, COLUMN_ID + "=?", whereArgs);
		db.close();
		Log.d("datta", "DBHelper addLocation done");
		Log.d("DBHelper", "addLocation closed");

	}

	/**
	 * Method to set current gamelevel
	 * 
	 * @param int level
	 */
	public void setGameLevel(int level) {
		db = getWritableDatabase();
		row.put("gamelevel", level);
		String[] whereArgs = { Long.toString(playerId) };
		db.update(TABLE_HIGHSCORE, row, COLUMN_ID + "=?", whereArgs);
		db.close();
		Log.d("DBHelper", "setGameLevel closed");

	}
	/**
	 * Method to obtain the player current gamelevel from database
	 * 
	 *  @return  int gamelevel
	 */
	public int getGameLevel() {
		db = getReadableDatabase();
		String[] columns = { COLUMN_GAMELEVEL, COLUMN_ID };
		String[] selectionArgs = { Long.toString(playerId) };
		Cursor c = db.query(TABLE_HIGHSCORE, columns, COLUMN_ID + "=?",
				selectionArgs, null, null, null);
		if (c.moveToFirst()) {
			return c.getInt(0);
		}
		return 0;
	}

	/**
	 * Increments user score.
	 * 
	 * @param int score
	 */
	public void addScore(int score) {
		db = getWritableDatabase();
		String[] columns = { COLUMN_SCORE };
		String[] selectionArgs = { Long.toString(playerId) };
		Cursor c = db.query(TABLE_HIGHSCORE, columns, COLUMN_ID + "=?",
				selectionArgs, null, null, null);
		int sumScore = score;
		Log.d("datta", "DBH 1 addPoints "+sumScore);
		if (c.moveToFirst()) {
			sumScore += c.getInt(0);
			Log.d("datta", "DBH 2 addPoints "+sumScore);
		}
		row.put("score", sumScore);
		db.update(TABLE_HIGHSCORE, row, COLUMN_ID + "=?", selectionArgs);
		db.close();
	}
	
	/**
	 * Reset score when player start new game.
	 */
	public void resetScore(){
		db = getWritableDatabase();
		String[] selectionArgs = { Long.toString(playerId) };
		row.put("score", 0);
		db.update(TABLE_HIGHSCORE, row, COLUMN_ID + "=?", selectionArgs);
		db.close();
	}
	
	/**
	 * Obtains player current score from database
	 * 
	 * @return int score
	 */
	public int getScore() {
		db = getWritableDatabase();
		String[] columns = { COLUMN_SCORE, COLUMN_ID };
		String[] selectionArgs = { Long.toString(playerId) };
		Cursor c = db.query(TABLE_HIGHSCORE, columns, COLUMN_ID + "=?",
				selectionArgs, null, null, null);
		if (c.moveToFirst()) {
			return c.getInt(0);
		}
		return 0;
	}

	/**
	 * Return a string with the players location.
	 * 
	 * @return String location
	 */
	public String getLocation() {
		db = getReadableDatabase();
		String[] columns = { COLUMN_LOCATION, COLUMN_ID };
		String[] selectionArgs = { Long.toString(playerId) };
		Cursor c = db.query(TABLE_HIGHSCORE, columns, COLUMN_ID + "=?",
				selectionArgs, null, null, null);
		if (c.moveToFirst()) {
			Log.d("DBHelper", "inside DBH getLocation IF");
			return c.getString(0);
		}
		Log.d("DBHelper", "inside DBH getLocation NOT-IF");
		return "";
	}

	/**
	 * Return the value of playerId wich is the players unique ID.
	 * 
	 * @return long playerID
	 */
	public long getPlayerId() {
		return playerId;
	}
	
	/**
	 * Saving the filename of player photo.
	 * 
	 * @param String filename
	 */
	public void setPhoto(String photoName) {
		db = getWritableDatabase();
		row.put("photo", photoName);
		String[] whereArgs = { Long.toString(playerId) };
		db.update(TABLE_HIGHSCORE, row, COLUMN_ID + "=?", whereArgs);
		db.close();
	}
	/**
	 * Method to delete player. Not used atm.
	 */
	public void deletePlayer(){
		db = getWritableDatabase();
		String[] whereArgs = { Long.toString(playerId) };
		db.delete(TABLE_HIGHSCORE, COLUMN_ID, whereArgs);
		db.close();
		Log.d("datta", "DBH deletet ???");
	}
}
