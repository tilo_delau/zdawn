package xxx.grupp6.zdawn.view.memberdata;

import java.util.Random;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

/**
 * 
 * @author Daniel
 * 
 *         Class to handle all info about the players. Name, points,
 *         geographical location and gamescore. It works like an interface
 *         between the gameplay and DBHelper.
 * 
 */

public class PlayerInfo {
	private static String name = "Dillinger";
	private static long playerId;
	private static int score = 0;
	private static String location = "Cyberspace";
	private static String photo = "Default";
	private static int ranking;
	private static int gameLevel;
	private static DBHelper dbh;
	private SharedPreferences sh;
	private Random rand;
	private Context context;

	public PlayerInfo(Context c) {
		this.context = c;
		dbh = new DBHelper(c);
		sh = c.getSharedPreferences("xxx.grupp6.zdawn.SETTING",
				Context.MODE_PRIVATE);
		setStats(checkPlayerExists());
	}

	/**
	 * Checks if there is a player threw sharedPreferences.
	 * 
	 * @return true if there is a sharedPreferense stored on phone.
	 */
	private boolean checkPlayerExists() {
		return sh.getLong("playerId", -1) >= 0;
	}

	/**
	 * Set the player stats. If there is none it'll create a new random name
	 * ZombieXXXX.
	 * 
	 * @param boolean if player exists or not
	 */
	private void setStats(boolean playerExist) {
		if (playerExist) {
			name = sh.getString("playerName", "JohnDoe");
			playerId = sh.getLong("playerId", 998);
			score = dbh.getScore();
			location = dbh.getLocation();
			gameLevel = dbh.getGameLevel();
			Log.d("datta", "setStats when True. Name: " + name + "  playerId "
					+ playerId + "  Location " + location + "  Photo " + photo
					+ "  Score " + score + "  Ranking " + ranking
					+ " GameLevel " + gameLevel);

		} else if (!playerExist) {
			rand = new Random();
			createPlayer("Zombie" + (rand.nextInt(1000) + 1));
			
			setStats(true);
			Log.d("datta", "setStat else if");

		}
	}

	/**
	 * Change the players name
	 * 
	 * @param String
	 *            newName
	 */
	public void changeName(String newName) {
		dbh.changeName(newName);
		name = newName;
	}

	/**
	 * Return the name stored from database. If database havent set the name the
	 * default value Dillinger is returned
	 * 
	 * @return player name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Send order to create a new player
	 * 
	 * @param String
	 *            name
	 */
	private void createPlayer(String name) {
		dbh.createNewPlayerDB(name);
	}

	/**
	 * The last known location of player
	 * 
	 * @return String city
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * Add location to database and set the location variable.
	 * 
	 * @param String
	 *            location
	 */
	public void setLocation(String loc) {
		dbh.addLocation(loc);
		location = loc;
	}

	/**
	 * Set the filename of players photo
	 * 
	 * @param String
	 *            photoName
	 */
	public void setPhoto(String photoName) {
		dbh.setPhoto(photoName);
	}

	/**
	 * Return the filename of players photo
	 * 
	 * @return String filename
	 */
	public String getPhoto() {
		return photo;
	}

	/**
	 * Return the current score.
	 * 
	 * @return int score
	 */
	public int getScore() {
		return score;
	}

	/**
	 * Returns the players ranking. I.e. the place of his score versus every
	 * player.
	 * 
	 * @return int rank
	 */
	public int getRanking() {
		// TODO not implemented yet.
		return 0;
	}

	/**
	 * Set what current gamelevel player has completed
	 * 
	 * @param int gamelevel
	 */
	public void setGameLevel(int newLevel) {
		dbh.setGameLevel(newLevel);
		gameLevel = newLevel;
	}

	/**
	 * Returns what gamelevel player is at.
	 * 
	 * @return int gamelevel
	 */
	public int getGameLevel() {
		return gameLevel;
	}

	/**
	 * Returns the unique player ID
	 * 
	 * @return long playerId
	 */
	public long getPlayerId() {
		Log.d("datta", "playerinfo_getPlayerId: " + playerId);
		Log.d("datta",
				"playerinfo_sharedPref id: " + sh.getLong("playerId", 432));
		return playerId;

	}

	/**
	 * Add score to players database and score.
	 * 
	 * @param int newScore
	 */
	public void addScore(int newScore) {
		dbh.addScore(newScore);
		score += newScore;
	}

	/**
	 * Sends a request to DBHelper to delete player.
	 */
	public void deletePlayer() {
		dbh.deletePlayer();

	}

	/**
	 * sends a reset score to 0 request to DBHandler
	 */
	public void resetScore() {
		dbh.resetScore();
		score = 0;
	}
}
