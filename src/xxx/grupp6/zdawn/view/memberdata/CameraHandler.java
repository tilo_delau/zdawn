package xxx.grupp6.zdawn.view.memberdata;

import java.io.File;
import java.io.FileOutputStream;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

//Outcommented section is for future purpose


/**
 * @author David Calderon
 * 
 * Class that handles the camera function.
 * Opens the android built-in camera, 
 * takes a photo, names the picture to the ID of the player,
 * stores it in a folder for this application,
 * and sends it to our database.  
 * calls openCamera() in onCreate
 *
 */
public class CameraHandler extends Activity {

	public static int pictureCount = 0;
	int TAKE_PICTURE = 0;
	private static final int CAMERA_REQUEST_CODE = 1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		openCamera();
	}
	
	/**
	 * Method that starts a new intent for the camera.   
	 * Opens up the camera.
	 * Moves on to the resultactivity when the camera intent is not null 
	 */
	private void openCamera() {
		Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if (cameraIntent.resolveActivity(getPackageManager()) != null) {
			startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == CAMERA_REQUEST_CODE) {
			// Checks and creates the folder zDawn, if not already created.
			File imgFolder = new File(Environment.getExternalStorageDirectory()
					+ File.separator + "zDawn");

			if (!imgFolder.exists()) {
				imgFolder.mkdirs();
				Log.d("CameraLog", "Folder created in: " + imgFolder.toString());
			}

			// Extract the bmp.
			if (data != null) {
				// Sets the name of the img to playerID.
				PlayerInfo pi = new PlayerInfo(this);
				String imgName = pi.getPlayerId() + ".jpg";
				Log.d("CameraLog", "Testing: " + pi.getPlayerId());
				File emuSdCard = Environment.getExternalStorageDirectory();
				String finalImgFolder = File.separator + "zDawn"
						+ File.separator;
				File imgDestination = new File(emuSdCard, finalImgFolder
						+ imgName);
				Log.d("CameraLog", "destination of image is: " + imgDestination);
				Log.d("CameraLog", "With the filename: " + imgName);

				pi.setPhoto(imgName);

				if (data.getExtras() != null) {
					Bitmap bmp = (Bitmap) data.getExtras().get("data");

					try {
						FileOutputStream out = new FileOutputStream(
								imgDestination);
						bmp.compress(Bitmap.CompressFormat.PNG, 100, out);

						// Sets the image from bitmap to our layout
						Bundle extras = data.getExtras();
						// Gets the image from bitmap
						@SuppressWarnings("unused")
						Bitmap image = (Bitmap) extras.get("data");
						out.flush();
						out.close();
					} catch (Exception e) {
						Log.e("CameraLog", "ERROR:" + e.toString());
					}
				}
			}
			finish();
		}
	}
}

// -----------------------------------
// FOR FULL IMAGE PURPOSE ONLY
// private void openCamera() {
// Intent cameraIntent = new Intent("android.media.action.IMAGE_CAPTURE");
// File file = new File(Environment.getExternalStorageDirectory()+File.separator
// + pictureCount+".jpg");
// cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
// startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE);
// }
//
//
// @Override
// protected void onActivityResult(int requestCode, int resultCode, Intent data)
// {
//
// if (requestCode == CAMERA_REQUEST_CODE) {
// pictureCount++;
// //Put the pic in a bmp object
// File file = new File(Environment.getExternalStorageDirectory()+File.separator
// + pictureCount+".jpg");
// Bitmap bmImg = bmDecoder(file.getAbsolutePath(), 1000, 700);
// }
// }
//
// public static Bitmap bmDecoder(String path, int reqWidth, int reqHeight) {
//
//
// //Check dimensions of the img....
// final BitmapFactory.Options options = new BitmapFactory.Options();
// options.inJustDecodeBounds = true;
// BitmapFactory.decodeFile(path, options);
//
// // Calculate w & h of img.
// final int imgH = options.outHeight;
// final int imgW = options.outWidth;
// options.inPreferredConfig = Bitmap.Config.RGB_565;
// int inImgSize = 1;
//
// if (imgH > reqHeight) {
// inImgSize = Math.round((float)imgH / (float)reqHeight);
// }
//
// int expectedWidth = imgW / inImgSize;
//
// if (expectedWidth > reqWidth) {
// //if(Math.round((float)width / (float)reqWidth) > inImgSize)
// inImgSize = Math.round((float)width / (float)reqWidth);
// }
//
// options.inImgSize = inImgSize;
// // dc bmp with inImgSize set
// options.inJustDecodeBounds = false;
// return BitmapFactory.decodeFile(path, options);
// }
// -----------------------------------