package xxx.grupp6.zdawn.view.memberdata;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

/**
 * Class to obtain player coordinate when finished a level. With Reversed
 * Geocoding we recieve the geographic location from coords.
 * 
 * 
 * @author Daniel -Grupp6
 * 
 */
public class LocationHandler implements
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener, LocationListener {

	private static LocationHandler instance = null;

	private LocationClient client;
	private LocationRequest request;
	private static Context c;
	private static Double longitude;
	private static Double latitude;
	private static String city;

	private LocationHandler(Context context) {
		c = context;
		client = new LocationClient(context, this, this);
		request = LocationRequest.create();
		request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		request.setInterval(5000);
		request.setFastestInterval(100);
		client.connect();

	}

	/**
	 * Instansiate this singleton
	 * 
	 * @param Context c
	 * @return instance
	 */
	public static LocationHandler getInstance(Context c) {
		if (instance == null) {
			instance = new LocationHandler(c);

		}
		return instance;
	}

	/**
	 * If connected location will recieve players coordiantions
	 * and save them into variables longitude and latitude.
	 * Then it sets city threw getMyLocationAdress()
	 * 
	 */
	public void checkLocation() {
		Log.d("datta", "checkLocation entered");

		if (client.isConnected()) {
			Toast.makeText(c, "Location: client is connected",
					Toast.LENGTH_LONG).show();
			Location l = client.getLastLocation();
			longitude = l.getLongitude();
			latitude = l.getLatitude();
			city = getMyLocationAddress();
			Log.d("datta", "CheckLocation:enter ; in ifsats city=" + city);
			Log.d("datta", "Long och lat: " + longitude + latitude);

		} else {
			Toast.makeText(c, "Huston.. we're not connected", Toast.LENGTH_LONG)
					.show();
		}
	}

	/**
	 * Set new longitude and latitude of location's changed.
	 * Then get the value of getMyLocationAdress() and save it to city.
	 */
	@Override
	public void onLocationChanged(Location l) {
		Log.d("datta", "Location:enter onLocationChanged");
		longitude = l.getLongitude();
		latitude = l.getLatitude();
		Log.d("datta", "Location: Long: " + longitude + " Lat: " + latitude);
		if (longitude > 1 && latitude > 1) {
			city = getMyLocationAddress();
			Log.d("datta",
					"Location:enter onLocationChanged onLOcationChange; in ifsats city="
							+ city);
			client.removeLocationUpdates(this);

		}

	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		Log.d("datta", "onConnectedFailed!");

	}

	@Override
	public void onConnected(Bundle arg0) {
		Log.d("datta", "onConnected!");
		client.requestLocationUpdates(request, this);

	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub

	}
	/**
	 * Uses Geocoder to obtain the physical adress of the longitude and latitude.
	 * Returns Cyberspace if connections failed and no long/lat exists.
	 * @return String city
	 */
	public String getMyLocationAddress() {
		Geocoder geocoder = new Geocoder(c, Locale.ENGLISH);
		try {
			// Place your latitude and longitude
			List<Address> addresses = geocoder.getFromLocation(latitude,
					longitude, 1);
			if (addresses != null) {
				Address fetchedAddress = addresses.get(0);
				for (int i = 0; i < fetchedAddress.getMaxAddressLineIndex(); i++) {
					Log.d("datta", addresses.get(i).getLocality());
					return addresses.get(i).getLocality();
				}
			}

			else
				Log.d("datta", "No location found..!");

		} catch (IOException e) {
			e.printStackTrace();
			Log.d("datta", "Catched!!");
		}
		return "Cyberspace";

	}
	/**
	 * Returns value of City
	 * 
	 * @return String city
	 */
	public String getCity() {
		return city;
	}

}
