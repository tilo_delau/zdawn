package xxx.grupp6.zdawn.view.memberdata;

import xxx.grupp6.zdawn.R;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 * @author Daniel
 * 
 *         Adapter to feed ListViews Items with data.
 */
public class HighscoreAdapter extends CursorAdapter {

	private LayoutInflater inflater;
	private SharedPreferences sp ;
	public HighscoreAdapter(Context context, Cursor c) {
		super(context, c, 0);
		sp = context.getSharedPreferences("xxx.grupp6.zdawn.SETTING", Context.MODE_PRIVATE);
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflater.inflate(R.layout.score_layout, null);
		return v;
	}
	/**
	 * We make views and obtain it's data from cursor and set the value.
	 */
	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		ImageView photoView = (ImageView) view.findViewById(R.id.score_photo_image);
		TextView nameText = (TextView) view.findViewById(R.id.score_name_text);
		TextView locationText = (TextView) view.findViewById(R.id.score_location_text);
		TextView scoreText = (TextView) view.findViewById(R.id.score_points_text);
		String fileName = cursor.getString(cursor.getColumnIndex("photo"));
		String name = cursor.getString(cursor.getColumnIndex("name"));
		String location = cursor.getString(cursor.getColumnIndex("location"));
		String score = cursor.getString(cursor.getColumnIndex("score"));
		
		if (fileName == null) {
			photoView.setImageResource(R.drawable.zic_launcher);
			Log.d("CameraLog", "Adapter, fileNameis NULL: "+fileName);
		} else {
			String file = context.getFilesDir().toString();
			Log.d("CameraLog", "Adapter, filename: "+file);
			Log.d("CameraLog", "Adapter, directory plus /zdawn: "+Environment.getExternalStorageDirectory()+"/zDawn");
			photoView.setImageBitmap(BitmapFactory.decodeFile(Environment.getExternalStorageDirectory() + "/zDawn/"+fileName));
		}
		nameText.setText(name);
		if(sp.getBoolean("showLocation", true)){
			locationText.setText(location);
		} else {
			locationText.setText("Ankeborg");
		}
		scoreText.setText(score);
	}

}
