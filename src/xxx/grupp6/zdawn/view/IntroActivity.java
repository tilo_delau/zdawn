package xxx.grupp6.zdawn.view;

import xxx.grupp6.zdawn.R;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * 
 * @author Tilo, 2014
 * Intro sequence with animated credits, game title and intro sound.
 * Clickable so player can skip intro
 *
 */
public class IntroActivity extends Activity {

	
	//NEW DAVID
	MediaPlayer mp;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_intro);
		removeActionBar();
		
//      SoundHandler was generating errors with the intensive animation use in intro so we have to hack right here /Tilo
        mp = MediaPlayer.create(this, R.raw.intro);
		mp.start();
		animateIntro();
					
	}
	private void animateIntro() {
				
		ImageView iBack;
		iBack = new ImageView(this);		
		final ImageView iTitle;
		iTitle = new ImageView(this);
		
		iTitle.setImageResource(R.drawable.zdawn);
		iTitle.setX(400);
		iTitle.setY(150);
		iTitle.setAlpha(0f);
		
		iBack.setImageResource(R.drawable.intro_2);
		iBack.setScaleX(4);
		iBack.setScaleY(4);
		iBack.setOnClickListener(listener);
				
		RelativeLayout lay = (RelativeLayout) findViewById(R.id.intro_back);
		lay.addView(iTitle); // iTitle added before iBack so its hidden behind iBack
		lay.addView(iBack);
		//	iTitle.setLayoutParams(lay.setGravity(Gravity.CENTER_HORIZONTAL)); Why doesn't this work?!
		
		// Important to start _after_ adding views in order to bring to front
		animateText();
		
		final ViewPropertyAnimator animator = iBack.animate();

		animator.setStartDelay(12000);
		animator.setDuration(9000);
		animator.scaleX(1);
		animator.scaleY(1);			
				
		Log.d("IntroActivity","First animation done");
		
		Runnable endAction = new Runnable() {
			Runnable endAction2 = new Runnable() {
				
			     public void run() {
			        Log.d("IntroActivity","Starting GamePlay from endAction2");
			        blink();
			       
			     }				
			 };
		     public void run() {
		    	 final ViewPropertyAnimator animator2 = iTitle.animate();
		    	 iTitle.bringToFront();
		    	 animator2.setDuration(5000);
		    	 animator2.alpha(1f);
		    	 animator2.withEndAction(endAction2);

		         Log.d("IntroActivity","Fading in Zombie Dawn title");
		     }
		 };

		 // callback to endAction after current animation ends
		animator.withEndAction(endAction);		
		animator.start();
				
	}

	private void blink() {

		Button v = (Button) findViewById(R.id.play_button);
		v.setVisibility(View.VISIBLE);
		v.bringToFront();
		v.setOnClickListener(listener);
		Animation anim = new AlphaAnimation(1.0f, 0.0f);
		anim.setDuration(400); // You can manage the blinking time with this parameter
		anim.setStartOffset(500);
		anim.setRepeatMode(Animation.REVERSE);
		anim.setRepeatCount(Animation.INFINITE);
		v.startAnimation(anim);

	}
	private void animateText() {
		final TextView tv1 = (TextView) findViewById(R.id.david);
		final TextView tv2 = (TextView) findViewById(R.id.tilo);
		final TextView tv3 = (TextView) findViewById(R.id.daniel);
		final TextView tv4 = (TextView) findViewById(R.id.markus);
		
		tv1.setAlpha(0f);
		tv2.setAlpha(0f);
		tv3.setAlpha(0f);
		tv4.setAlpha(0f);

				
		final ViewPropertyAnimator animatorTv1 = tv1.animate();
		final ViewPropertyAnimator animatorTv2 = tv2.animate();
		final ViewPropertyAnimator animatorTv3 = tv3.animate();
		final ViewPropertyAnimator animatorTv4 = tv4.animate();
		
		Runnable endActionText = new Runnable() {
			
		     public void run() {
		 		animatorTv1.alpha(0f);
				animatorTv2.alpha(0f);
				animatorTv3.alpha(0f);		
				animatorTv4.alpha(0f);
				
				animatorTv1.setDuration(4000);
				animatorTv2.setDuration(4000);
				animatorTv3.setDuration(4000);
				animatorTv4.setDuration(4000);

				animatorTv1.setStartDelay(1000);
				animatorTv2.setStartDelay(2000);
				animatorTv3.setStartDelay(3000);
				animatorTv4.setStartDelay(4000);
						    	 

		         Log.d("IntroActivity","Fading out text");
		     }
		 };
		 
		animatorTv1.alpha(1f);
		animatorTv2.alpha(1f);
		animatorTv3.alpha(1f);		
		animatorTv4.alpha(1f);
		
		
		animatorTv1.setDuration(4000);
		animatorTv2.setDuration(4000);
		animatorTv3.setDuration(4000);
		animatorTv4.setDuration(4000);
		
		animatorTv1.setStartDelay(2000);
		animatorTv2.setStartDelay(3000);
		animatorTv3.setStartDelay(4000);
		animatorTv4.setStartDelay(5000);

		
		animatorTv1.start();
		animatorTv2.start();
		animatorTv3.start();
		animatorTv4.withEndAction(endActionText); // need the last one to call back
		animatorTv4.start();
		
		tv1.bringToFront();
		tv2.bringToFront();
		tv3.bringToFront();
		tv4.bringToFront();
	}

	/*
	 * menu options. round about way to start game play
	 */
	public void menuOptions() {

		Intent intent = new Intent(this, GamePlayActivity.class);
		startActivity(intent);
//		playMusic(3);
		finish();
		
	}
	private OnClickListener listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			menuOptions();
		}
	};

	private void removeActionBar() {
        //Get rid of ActionBar
        ActionBar  actionBar = getActionBar();
        actionBar.hide();
        //Get rid of notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.intro, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	//NEW DAVID
	protected void onStop() {
		super.onStop();
		mp.stop();
	}
}
