	package xxx.grupp6.zdawn.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import xxx.grupp6.zdawn.model.zombies.AbstractZombie;
import xxx.grupp6.zdawn.model.zombies.NormalZombie;
import xxx.grupp6.zdawn.model.zombies.ToughZombie;
import android.util.Log;

/**
 * Class to manage communication between game views and game logics.
 * 
 * @author Markus -Grupp6/ITHS
 *
 */

public class GameEngine implements PropertyChangeListener{
	
	private static GameEngine instance = null;
	
	private PropertyChangeSupport change;
	private Player p;
	private ArrayList<AbstractZombie> zombieList;
	private Random rand;
	private Timer timer;
	private TimerTask zombieSpawnTimerTask;
	private int level;
	private int zombieAmount;
	private int zombiesInList;
	private int spawnTime;
	private int types;
	private int timerCounter;
	private int zombieKillScore;
	private int molotovKillScore;
	private boolean stopTimer;
	
	private GameEngine() {
		rand = new Random();
	}
	/**
	 * Checks if there already is an instance of the class. If there is none, create one.
	 * @return instance of class
	 */
	public static GameEngine getInstance() {
		if(instance == null) {
			instance = new GameEngine();
		}
		return instance;
	}
	/**
	 * Resets values for new game.
	 */
	public void newGame() {
		p = new Player();
		p.addPropertyChangeListener(this);
		zombieList = new ArrayList<AbstractZombie>();
		timerCounter = 0;
		stopTimer = false;
		zombieKillScore = 0;
		molotovKillScore = 0;
		zombiesInList = 0;
		change = new PropertyChangeSupport(this);
	}
	/**
	 * Get zombieKillScore (Number of zombies killed).
	 * @return zombieKillScore
	 */
	public int getZombieKillScore() {
		return zombieKillScore;
	}
	/**
	 * Get molotovKillScore (Number of zombies killed with molotov).
	 * @return molotovKillScore
	 */
	public int getMolotovKillScore() {
		return molotovKillScore;
	}
	/**
	 * Get the current level.
	 * @return current level
	 */
	public int getLevel() {
		return level;
	}
	/**
	 * Creates a new zombie (80% normal, 20% tough).
	 * @return zombie object
	 */
	public AbstractZombie createZombie() {
		AbstractZombie z;
		int randomNumber = rand.nextInt(10)+1;
		if(randomNumber > 8 && types > 1) {
			z = new ToughZombie();
		} else {
			z = new NormalZombie();
		}
		zombieList.add(z);
		zombiesInList++;
		Log.d("markus", "z in list: "+ zombiesInList);
		z.addPropertyChangeListener(this);
		return z;
	}
	public ArrayList<AbstractZombie> getZombieList() {
		return zombieList;
	}
	public int getPositionInList(AbstractZombie z) {
		for(int i = 0; i < zombieList.size(); i++) {
			if(zombieList.get(i).equals(z)) {
				return i;
			}
		}
		return -1;
	}
	/**
	 * Set the time between zombie spawns and the amount of zomibes on the level.
	 * @param int level
	 */
	public void setZombieSpawnTimer(int level) {
		//zombie count = level 1 amount, respawn speed = level 1 spawn time, zombie types = level 1 types.
		this.level = level;
		Log.d("GE", "level: "+this.level);
		zombieAmount = 2+level;
		Log.d("GE", "zombieAmount: "+zombieAmount);
		spawnTime = 4300-(level*300);
		types = 2;
		if(spawnTime < 1500) {
			spawnTime = 1500;
		}
	}
	/**
	 * Create and start the spawn timer
	 */
	public void startZombieSpawnTimer() {
		Log.d("GE", "stopTimer1: "+stopTimer);
		timer = new Timer();
		setZombieSpawnTimerTask();
		zombieSpawnTimer();
		Log.d("GE", "start timer");
	}
	/**
	 * Start zombie attack timer
	 * @param AbstractZombie z
	 */
	public void startPlayerDmgTimer(final AbstractZombie z) {
		new Timer().scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
					Log.d("GE","startplayerdmgtimer called");
				if(p.getHp() < 1) {
					change.firePropertyChange("playerDead", true, false);
					this.cancel();
				} else if(stopTimer) {
					this.cancel();
				} else if(z.isDead()) {
					Log.d("GE","Z is dead");
					this.cancel();
				} else {
					Log.d("GE","eat player");
					p.takeDamage(z.getDmg());
					change.firePropertyChange("playerAttacked", true, false);
				}
			}
		}, 0, z.getSpeed());
	}
	/**
	 * Sets boolean to stop timers
	 */
	public void stopTimer() {
		stopTimer = true;
	}
	/**
	 * Damage the zombie with currently equipped player weapon.
	 * @param AbstractZombe z
	 */
	public void takeDamage(AbstractZombie z) {
		if(!z.isDead()) {
			z.takeDamage(p.RangedAttack());
			Log.d("markus", p.getCurrentWeaponAmmo()+"");
		}
	}
	/**
	 * Swap weapon.
	 */
	public void swapWeapon() {
		p.swapWeapon();
	}
	/**
	 * Throw molotov and kill all zombies on the screen.
	 * @return true if player had molotovs left.
	 */
	public boolean throwMolotov() {
		for(int i = 0; i < zombiesInList; i++) {
			if(!zombieList.get(i).isDead()) {
				Log.d("markus", "throw molotov game engine. not dead");
				zombieList.get(i).takeFatalDamage();
				molotovKillScore++;
			}
		}
		return true;
	}
	/**
	 * Kill all zombies without firing "zombieDead" event.
	 */
	public void killAllZombies() {
		for(int i = 0; i < zombiesInList; i++) {
			if(!zombieList.get(i).isDead()) {
				Log.d("markus", "kill all zombies");
				zombieList.get(i).killOnLose();
			}
		}
		change.firePropertyChange("killAll", true, false);
	}
	/**
	 * Use ammo without hitting.
	 */
	public void missShot() {
		p.RangedAttack();
	}
	/**
	 * Reloads current weapon.
	 */
	public void reload() {
		p.reload();
	}
	/**
	 * Check if the player is currently reloading.
	 * @return true if player is reloading.
	 */
	public boolean isReloading() {
		return p.isReloading();
	}
	/**
	 * Zombie attack. Player takes damage.
	 * @param AbstractZombie z
	 */
	public void zombieAttack(AbstractZombie z) {
		p.takeDamage(z.getDmg());
	}
	/**
	 * Get zombie max hp.
	 * @param AbstractZombie z
	 * @return zombie max hp.
	 */
	public int getMaxHp(AbstractZombie z) {
		return z.getMaxHp();
	}
	/**
	 * Get current hp of zombie.
	 * @param AbstractZombie z
	 * @return current zombie hp
	 */
	public int getHp(AbstractZombie z) {
		return z.getHp();
	}
	/**
	 * Check if zombie is dead.
	 * @param AbstractZombie z
	 * @return true if zombie is dead.
	 */
	public boolean isDead(AbstractZombie z) {
		return z.isDead();
	}
	/**
	 * Get player hp.
	 * @return player hp
	 */
	public int getPlayerHp() {
		return p.getHp();
	}
	/**
	 * Get player max hp.
	 * @return player max hp
	 */
	public int getPlayerMaxHp() {
		return p.getMaxHp();
	}
	/**
	 * Get max ammo of current weapon.
	 * @return max ammo of current weapon
	 */
	public int getMaxAmmo() {
		return p.getCurrentWeaponMaxAmmo();
	}
	/**
	 * Get current weapon ammo.
	 * @return current weapon ammo
	 */
	public int getAmmo() {
		return p.getCurrentWeaponAmmo();
	}
	/**
	 * Get weapon type of current weapon.
	 * @return weapon type
	 */
	public int getWeaponType() {
		return p.getWeaponType();
	}
	/**
	 * Listens to events:
	 * "reloading": sends it's own reloading event.
	 * "zombieDead": Sends "zombieDead" event and checks if all zombies are dead. If they are, send "levelComplete" event after a short delay.
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName().equals("reloading")) {
			Log.d("markus", "property change game engine " +event.getNewValue());
			change.firePropertyChange("reloading", true, false);
		}
		if(event.getPropertyName().equals("zombieDead")) {
			zombieKillScore++;
			change.firePropertyChange("zombieDead", true, false);
			if(zombieKillScore == zombieAmount) {
				new Timer().schedule(new TimerTask() {
					@Override
					public void run() {
						change.firePropertyChange("levelComplete", true, false);
					}
				}, 1000);
			}
		}
	}
	/**
	 * Adds a PropertyChangeListener to this class.
	 * @param PropertyChangeListener l
	 */
	public void addPropertyChangeListener(PropertyChangeListener l) {
		change.addPropertyChangeListener(l);
	}
	/**
	 * Removes a PropertyChangeListener from this class.
	 * @param PropertyChangeListener l
	 */
	public void removePropertyChangeListener(PropertyChangeListener l) {
		change.removePropertyChangeListener(l);
	}
	
	private void zombieSpawnTimer() {
		Log.d("GE", "stopTimer2: "+stopTimer);
		timer.scheduleAtFixedRate(zombieSpawnTimerTask, 3000, spawnTime);
	}
	
	private void setZombieSpawnTimerTask() {
		zombieSpawnTimerTask = new TimerTask() {
			@Override
			public void run() {
				Log.d("GE", "stopTimer3: "+stopTimer);
				Log.d("GE", "start of timer task");
				if(stopTimer) {
					Log.d("GE", "stopTimer4: "+stopTimer);
					clearTimer();
				} else {
					timerCounter++;
					Log.d("markus", "spawnZombie");
					change.firePropertyChange("spawnZombie", true, false);
				}
				if(timerCounter == zombieAmount || stopTimer) {
					clearTimer();
				}
				//change.firePropertyChange("newZombie", null, createZombie());  hel zombie
			}
		};
	}
	
	private void clearTimer() {
		zombieSpawnTimerTask.cancel();
		timer.cancel();
		timer.purge();
		timerCounter = 0;
	}
	
}








