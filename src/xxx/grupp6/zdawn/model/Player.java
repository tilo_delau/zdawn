package xxx.grupp6.zdawn.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import xxx.grupp6.zdawn.model.weapons.BasicHammer;
import xxx.grupp6.zdawn.model.weapons.MeleeWeapon;
import xxx.grupp6.zdawn.model.weapons.Pistol;
import xxx.grupp6.zdawn.model.weapons.RangedWeapon;
import xxx.grupp6.zdawn.model.weapons.Shotgun;
import android.util.Log;
/**
 * Class that contains information about the player in-game.
 * 
 * @author Markus
 *
 */
public class Player implements PropertyChangeListener{
	public Player() {
		maxHp = 50;
		hp = maxHp;
		molotovStash = 1;
		pistol = new Pistol();
		shotgun = new Shotgun();
		rangedW = pistol;
		meleeW = new BasicHammer();
		pistol.addPropertyChangeListener(this);
		shotgun.addPropertyChangeListener(this);
		change = new PropertyChangeSupport(this);
	}
	
	private PropertyChangeSupport change;
	private int hp;
	private int maxHp;
	private int molotovStash;
	private RangedWeapon pistol;
	private RangedWeapon shotgun;
	private RangedWeapon rangedW;
	private MeleeWeapon meleeW;
	/**
	 * Switches to the other weapon.
	 */
	public void swapWeapon() {
		if(rangedW.getType() == 1) {
			rangedW = shotgun;
		} else if (rangedW.getType() == 2) {
			rangedW = pistol;
		}
	}
	/**
	 * Consumes ammo and returns damage of the currently equipped weapon.
	 * @return damage of weapon
	 */
	public int RangedAttack() {
		rangedW.useAmmo();
		return rangedW.getDmg();
	}
	/**
	 * Method to comsume a molotov when thrown.
	 * @return true if player has molotovs left
	 */
	public boolean throwMolotov() {
		if(molotovStash > 0) {
			molotovStash--;
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Get current hp of player.
	 * @return current hp of player
	 */
	public int getHp() {
		return hp;
	}
	/**
	 * Get max hp of player.
	 * @return max hp of player
	 */
	public int getMaxHp() {
		return maxHp;
	}
	/**
	 * Takes damage equivalent to the parameter.
	 * @param int dmg 
	 */
	public void takeDamage(int dmg) {
		hp = hp - dmg;
	}
	/**
	 * Get ammo of currently equipped weapon.
	 * @return ammo of currently equipped weapon
	 */
	public int getCurrentWeaponAmmo() {
		return rangedW.getAmmo();
	}
	/**
	 * Get max ammo of currently equipped weapon.
	 * @return max ammo of currently equipped weapon
	 */
	public int getCurrentWeaponMaxAmmo() {
		return rangedW.getMaxAmmo();
	}
	/**
	 * Get type of weapon equipped.
	 * @return weapon type
	 */
	public int getWeaponType() {
		return rangedW.getType();
	}
	/**
	 * Checks if player is reloading. If not, start reload.
	 */
	public void reload() {
		if(!rangedW.isReloading()) {
			Log.d("markus", "reloading..");
			rangedW.reloadDelay();
		}
	}
	/**
	 * Check if player is reloading.
	 * @return true if player is reloading
	 */
	public boolean isReloading() {
		return rangedW.isReloading();
	}
	/**
	 * Receives and forwards "reload" event.
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName().equals("reloading")) {
			Log.d("markus", "property change player " +event.getNewValue());
			change.firePropertyChange("reloading", true, false);
		}
	}
	/**
	 * Adds a PropertyChangeListener to this class.
	 * @param PropertyChangeListener l
	 */
	public void addPropertyChangeListener(PropertyChangeListener l) {
		change.addPropertyChangeListener(l);
	}
	/**
	 * Removes a PropertyChangeListener from this class.
	 * @param PropertyChangeListener l
	 */
	public void removePropertyChangeListener(PropertyChangeListener l) {
		change.removePropertyChangeListener(l);
	}
}
