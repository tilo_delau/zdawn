package xxx.grupp6.zdawn.model.weapons;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Timer;
import java.util.TimerTask;

import android.util.Log;


/**
 * Class that contains all information about one weapon. Values are set from subclass constructor.
 * 
 * @author Markus
 *
 */
public class RangedWeapon {
	
	private PropertyChangeSupport change;
	private int dmg;
	private int maxAmmo;
	private int ammo;
	private int reloadTime;
	private Timer timer;
	private boolean reloading;
	private int type;
	
	public RangedWeapon(int dmg, int maxAmmo, int reloadTime, int type) {
		this.dmg = dmg;
		this.maxAmmo = maxAmmo;
		ammo = maxAmmo;
		this.reloadTime = reloadTime*1000;
		timer = new Timer();
		this.type = type;
		change = new PropertyChangeSupport(this);
	}
	/**
	 * Get damage of the weapon.
	 * @return weapon damage
	 */
	public int getDmg() {
		return dmg;
	}
	/**
	 * Get current ammo of the weapon.
	 * @return weapon ammo
	 */
	public int getAmmo() {
		return ammo;
	}
	/**
	 * Get max ammo of weapon.
	 * @return max ammo
	 */
	public int getMaxAmmo() {
		return maxAmmo;
	}
	/**
	 * Get type of weapon.
	 * @return weapon type
	 */
	public int getType() {
		return type;
	}
	/**
	 * Lowers ammo by 1.
	 */
	public void useAmmo() {
		ammo = ammo - 1;
	}
	/**
	 * Checks if weapon is reloading.
	 * @return true if weapon is reloading
	 */
	public boolean isReloading() {
		return reloading;
	}
	/**
	 * Starts a timer and reloads weapon after "reloadTime" milliseconds.
	 */
	public void reloadDelay() {
		reloading = true;
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				reload();
				reloading = false;
			}
		}, reloadTime);
	}
	/**
	 * Sets ammo equal to max ammo.
	 */
	public void reload() {
		//delay (reloadTime) 
		ammo = maxAmmo;
		Log.d("markus", "reload finished");
		change.firePropertyChange("reloading", true, false);
	}
	/**
	 * Adds a PropertyChangeListener to this class.
	 * @param PropertyChangeListener l
	 */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        change.addPropertyChangeListener(l);
    }
    /**
	 * Removes a PropertyChangeListener from this class.
	 * @param PropertyChangeListener l
	 */
    public void removePropertyChangeListener(PropertyChangeListener l) {
        change.removePropertyChangeListener(l);
    }
}
