package xxx.grupp6.zdawn.model.weapons;
/**
 * Subclass to RangedWeapon. Sends stats from constructor to super constructor.
 * 
 * @author Markus
 *
 */
public class Pistol extends RangedWeapon {

	public Pistol() {
		super(2, 8, 3, 1); //dmg, ammo, reloadTime, type
	}

}
