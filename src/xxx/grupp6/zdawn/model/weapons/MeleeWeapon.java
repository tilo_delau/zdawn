package xxx.grupp6.zdawn.model.weapons;
/**
 * Class for melee weapons. Not in use.
 * 
 * @author Markus
 *
 */
public class MeleeWeapon {
	
	private int dmg;
	
	public MeleeWeapon(int dmg) {
		this.dmg = dmg;
	}

	public int getDmg() {
		return dmg;
	}
}
