package xxx.grupp6.zdawn.model.weapons;
/**
 * Subclass to RangedWeapon. Sends stats from constructor to super constructor.
 * 
 * @author Markus
 *
 */
public class Shotgun extends RangedWeapon {

	public Shotgun() {
		super(6, 5, 5, 2); //dmg, ammo, reload time, type
	}

}
