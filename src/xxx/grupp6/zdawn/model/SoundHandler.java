package xxx.grupp6.zdawn.model;

import java.io.IOException;

import java.util.Random;

import xxx.grupp6.zdawn.R;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.util.Log;


/**
 * @author David Calderon
 * 
 * @param 
 * musicSwitch true = on, false=off
 */
public class SoundHandler {
	
	public static MediaPlayer mp = new MediaPlayer();
	public static boolean onTrack = true;
	public static boolean next = false;
	public static boolean musicSwitch = true;
	public static boolean switchedOff = false;
	MediaPlayer fxMp;
	MediaPlayer miscFxMp;
	
	
	/**
	 * Method that crates a mediaplayer and gets the music.
	 * Switches the int sent to this method 
	 * Creates a mediaplayer with a resource, depending on the int sent to this method.
	 * Starts the mediaplayer
	 * Loads SharedPreferences to get a boolean depending on another activity.
	 * 
	 * @param c - this is the context
	 * @param resID - uses in from the activity calling this method for the switch case.
	 * @return - MediaPlayer mp
	 * @throws IOException - throws on error
	 * @see IOException
	 */
	public static MediaPlayer getMusic(Context c, int resID) throws IOException {
		SharedPreferences sh = c.getSharedPreferences("xxx.grupp6.zdawn.SETTING", Context.MODE_PRIVATE);
		musicSwitch = sh.getBoolean("MusicSwitchState", true);;
		Log.e("BOOLSTATE","BOOL IS: " + musicSwitch);

		boolean looping = true;
		if (mp != null && mp.isPlaying()) {
			mp.stop();
			mp.release();
			mp = null;
		} 
		
		
		switch (resID) {
		case 1:     
			if(mp != null) {
				mp.reset();
				mp.release();
				mp = null;
			}
			mp = MediaPlayer.create(c, R.raw.intro);
			mp.start();
			break;  

		case 2:    
			if (onTrack) {
			}
			else if(mp != null) {
				mp.reset();
				mp.release();
				mp = null;
			}
			if (musicSwitch == true) {
			mp = MediaPlayer.create(c, R.raw.menu);
			mp.setLooping(looping);
			mp.start();
			onTrack = false;
			} else if (musicSwitch == true) {
				break;
			}
			break;                

		case 3:      
			if(mp != null) {
				mp.reset();
				mp.release();
				mp = null;
			}
			if (musicSwitch == true) {
			mp = MediaPlayer.create(c, R.raw.stage1);
			mp.setLooping(looping);
			mp.start();
			} else if (musicSwitch == true) {
				break;
			}
			break;                

		case 4:      
			if(mp != null) {
				mp.reset();
				mp.release();
				mp = null;
			}
			if (musicSwitch == true) {
			mp = MediaPlayer.create(c, R.raw.stage2);
			mp.setLooping(looping);
			mp.start();
			} else if (musicSwitch == true) {
				break;
			}
			break;  
			
		case 5:      
			if(mp != null) {
				mp.reset();
				mp.release();
				mp = null;
			}
			if (musicSwitch == true) {
			mp = MediaPlayer.create(c, R.raw.stage3);
			mp.setLooping(looping);
			mp.start();
			} else if (musicSwitch == true) {
				break;
			}
			break;  
		}
		return mp;
	}

	//Sounds for weapons and FX for weapons.. 
	//Word within "" is the trigger. 
	/**
	 * Method that creates a mediaplayer
	 * plays soundfx from res folder.
	 * uses string as a trigger to play the fx depending on the string sent to this method. 
	 * sets boolean next = true when the sound is finished playing, so the next sound can play.
	 * 
	 * uses onCompletionlistener to release the mediaplayer when completed. 
	 * 
	 * @param context - The context.
	 * @param fx a string to trigger the fx within the quotations, depending on the strings value.
	 */
	public static void getFx(Context context, String fx){

		if (fx.equals("pistol")) {
			MediaPlayer fxMp;
			fxMp = MediaPlayer.create(context, R.raw.fx_pistol);
			fxMp.start();
			fxMp.setOnCompletionListener(new OnCompletionListener(){
				public void onCompletion(MediaPlayer fxMp) {
					fxMp.release();
				};
			});
			next = true;

		} 	else if (fx.equals("shotgun")) {
			MediaPlayer fxMp;
			fxMp = MediaPlayer.create(context, R.raw.fx_shotgun);
			fxMp.start();
			fxMp.setOnCompletionListener(new OnCompletionListener(){

				public void onCompletion(MediaPlayer fxMp) {
					fxMp.release();
				};
			});
			next = true;

		} 	else if (fx.equals("lighter")) {
			MediaPlayer fxMp;
			fxMp = MediaPlayer.create(context, R.raw.fx_lighter);
			fxMp.start();
			fxMp.setOnCompletionListener(new OnCompletionListener(){

				public void onCompletion(MediaPlayer fxMp) {
					fxMp.release();
				};
			});
			next = true;

		} else if (fx.equals("lightmolotov")) {
			MediaPlayer fxMp;
			fxMp = MediaPlayer.create(context, R.raw.fx_molotovon);
			fxMp.start();
			fxMp.setOnCompletionListener(new OnCompletionListener(){

				public void onCompletion(MediaPlayer fxMp) {
					fxMp.release();
				};
			});
			next = true;

		} else if (fx.equals("throw")) {
			MediaPlayer fxMp;
			fxMp = MediaPlayer.create(context, R.raw.fx_throw);
			fxMp.start();
			fxMp.setOnCompletionListener(new OnCompletionListener(){

				public void onCompletion(MediaPlayer fxMp) {
					fxMp.release();
				};
			});
			next = true;

		} else if (fx.equals("breakglass")) {
			MediaPlayer fxMp;
			fxMp = MediaPlayer.create(context, R.raw.fx_breakglass);
			fxMp.start();
			fxMp.setOnCompletionListener(new OnCompletionListener(){

				public void onCompletion(MediaPlayer fxMp) {
					fxMp.release();
				};
			});
			next = true;

		} else if (fx.equals("burn")) {
			MediaPlayer fxMp;
			fxMp = MediaPlayer.create(context, R.raw.fx_burn);
			fxMp.start();
			fxMp.setOnCompletionListener(new OnCompletionListener(){

				public void onCompletion(MediaPlayer fxMp) {
					fxMp.release();
				};
			});
			next = true;
			
		}	else if (fx.equals("bodysplash")) {
			MediaPlayer fxMp;
			fxMp = MediaPlayer.create(context, R.raw.bodysplash);
			fxMp.start();
			fxMp.setOnCompletionListener(new OnCompletionListener(){

				public void onCompletion(MediaPlayer fxMp) {
					fxMp.release();
				};
			});
			next = true;
			
		}	else if (fx.equals("eating")) {
			MediaPlayer fxMp;
			fxMp = MediaPlayer.create(context, R.raw.eating);
			fxMp.start();
			fxMp.setOnCompletionListener(new OnCompletionListener(){

				public void onCompletion(MediaPlayer fxMp) {
					fxMp.release();
				};
			});
			next = true;
			
		}	else if (fx.equals("reload")) {
			MediaPlayer fxMp;
			fxMp = MediaPlayer.create(context, R.raw.fx_reload);
			fxMp.start();
			fxMp.setOnCompletionListener(new OnCompletionListener(){

				public void onCompletion(MediaPlayer fxMp) {
					fxMp.release();
				};
			});
			next = true;
			
		}	else if (fx.equals("zombiedie")) {
			MediaPlayer fxMp;
			fxMp = MediaPlayer.create(context, R.raw.fx_zombiedie);
			fxMp.start();
			fxMp.setOnCompletionListener(new OnCompletionListener(){

				public void onCompletion(MediaPlayer fxMp) {
					fxMp.release();
				};
			});
			next = true;
		}					
		
	}
	
	/**
	 * Method that plays a fx at a random order when called, 
	 * 
	 * store the sounds destination from res folder in int[]
	 * creates a mediaplayer with a random int, getting a random sound from the int[]
	 * check if next = true, if next, continues to next sound. 
	 * uses OnCompletionlistener
	 * @param context - The context
	 */
	public static void getMoan(Context context) {
		Random r = new Random();
		MediaPlayer zombieMoanMp;
		int[] rawRef = {R.raw.fx_moan1,
				R.raw.fx_moan2,
				R.raw.fx_moan3,
				R.raw.fx_moan4,
				R.raw.fx_moan5,
				R.raw.fx_moan6,
				R.raw.fx_moan7,
				R.raw.fx_moan8,
				R.raw.fx_moan9,
		};

		zombieMoanMp = MediaPlayer.create(context, rawRef[r.nextInt(rawRef.length)]);
		zombieMoanMp.setAudioStreamType(AudioManager.STREAM_MUSIC);
		zombieMoanMp.setVolume(1, 1);
		if (next) {
			zombieMoanMp.start();
			zombieMoanMp.setOnCompletionListener(new OnCompletionListener(){

				public void onCompletion(MediaPlayer zombieMoanMp) {
					zombieMoanMp.release();
				};
			});
		}
	}

	/**
	 * Stops the sound for the mediaplayer mp
	 */
	public void stopSound() {
		SoundHandler.mp.stop();
		SoundHandler.mp.reset();
		SoundHandler.mp.release();
	}

	/**
	 * Stops the sound for the mediaplayer mp
	 */
	public static void onBackStopSound() {
		if(SoundHandler.mp != null && SoundHandler.mp.isPlaying())
			SoundHandler.mp.stop();
	}
}