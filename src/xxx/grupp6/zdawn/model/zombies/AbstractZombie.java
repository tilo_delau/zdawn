package xxx.grupp6.zdawn.model.zombies;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Random;
/**
 * Class to define a zombie object. Gets initial values from subclass.
 * 
 * @author Markus
 *
 */
public abstract class AbstractZombie {
	
	private PropertyChangeSupport change;
	private int hp;
	private int maxHp;
	private boolean dead;
	private int dmg;
	private int speed;
	private int animation;
	private int type;
	private Random rand;
	
	public AbstractZombie(int maxHp, int dmg, int speed, int animation, int type) {
		change = new PropertyChangeSupport(this);
		this.maxHp = maxHp;
		hp = maxHp;
		this.dmg = dmg;
		this.speed = speed * 1000;
		this.animation = animation;
		this.type = type;
		rand = new Random();
	}
	/**
	 * Get current hp.
	 * @return current hp
	 */
	public int getHp() {
		return hp;
	}
	/**
	 * Set hp.
	 * @param int hp
	 */
	public void setHp(int hp) {
		this.hp = hp;
	}
	/**
	 * Get max hp.
	 * @return max hp
	 */
	public int getMaxHp() {
		return maxHp;
	}
	/**
	 * Check if zombie is dead.
	 * @return true if zombie is dead
	 */
	public boolean isDead() {
		return dead;
	}
	/**
	 * Sets the boolean dead to true and fires a "zombieDead" event.
	 */
	public void setDead() {
		dead = true;
		change.firePropertyChange("zombieDead", true, false);
	}
	/**
	 * Get the amount of damage the zombie can do.
	 * @return damage
	 */
	public int getDmg() {
		return rand.nextInt(5)+dmg;
	}
	/**
	 * Get zombie speed
	 * @return speed
	 */
	public int getSpeed() {
		return speed;
	}
	/**
	 * Get zombie type
	 * @return zombie type
	 */
	public int getType() {
		return type;
	}
	/**
	 * Calls setDead(). 
	 */
	public void takeFatalDamage() {
		setDead();
	}
	/**
	 * Sets the boolean dead to true.
	 */
	public void killOnLose() {
		dead = true;
	}
	/**
	 * Abstract method that allows each specific zombie to take damage in its own way.
	 * @param int dmg
	 */
	public abstract void takeDamage(int dmg);
	/**
	 * Adds a PropertyChangeListener to this class.
	 * @param PropertyChangeListener l
	 */
	public void addPropertyChangeListener(PropertyChangeListener l) {
		change.addPropertyChangeListener(l);
	}
	/**
	 * Removes a PropertyChangeListener from this class.
	 * @param PropertyChangeListener l
	 */
	public void removePropertyChangeListener(PropertyChangeListener l) {
		change.removePropertyChangeListener(l);
	}
}
