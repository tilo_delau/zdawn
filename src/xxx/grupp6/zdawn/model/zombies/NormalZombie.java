package xxx.grupp6.zdawn.model.zombies;

/**
 * Subclass to AbstractZombie. Sends stats from constructor to super constructor.
 * Has its own method to take damage.
 * @author Markus
 *
 */
public class NormalZombie extends AbstractZombie {
	
	public NormalZombie() {
		super(6, 7, 2, 1, 1); //hp, dmg, speed, animation, type
	}
	/**
	 * Take damage equivalent to the incoming argument. If hp < 1, call setDead().
	 */
	@Override
	public void takeDamage(int dmg) {
		setHp(getHp()-dmg);
		if (getHp() < 1) {
			setDead();
		}
	}
}
