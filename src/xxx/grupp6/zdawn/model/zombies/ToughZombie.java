package xxx.grupp6.zdawn.model.zombies;

/**
 * Subclass to AbstractZombie. Sends stats from constructor to super constructor.
 * Has its own method to take damage.
 * @author Markus
 *
 */
public class ToughZombie extends AbstractZombie {

	public ToughZombie() {
		super(10, 10, 3, 1, 2); //hp, dmg, speed, animation, type
	}
	/**
	 * Take damage equivalent to the incoming argument-1. If hp < 1, call setDead().
	 */
	@Override
	public void takeDamage(int dmg) {
		setHp(getHp()-(dmg-1));
		if (getHp() < 1) {
			setDead();
		}
	}

}
