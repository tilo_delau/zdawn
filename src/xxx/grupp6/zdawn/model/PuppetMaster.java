/*
 * Copyright (C) 2014 Group 6, David, Tilo, Daniel, Markus 
 *
 */

package xxx.grupp6.zdawn.model;

import java.util.Random;

/**
 * @author Tilo
 * Generates coordinate-based paths of the zombies
 */
public class PuppetMaster {
	private int startPosX;
	private int startPosY;

	private int endPosX;
	private int endPosY;

	private int midPosX;
	private int midPosY;


	/**
	 * Randomly selects 1 path from 3 for the zombie to walk
	 */
	public PuppetMaster() {
		
		Random rand = new Random();
		
		int path = rand.nextInt(3);
		
		switch (path) {
			case 0: 	setPath1(); 	break;
			case 1: 	setPath2();		break;
			case 2:		setPath3(); 	break;
			default: 	setPath1();  	break;
		} 

		// Remove comments to overwrite switch to test
/*
		setStartPosX(1050);
		setMidPosX(500);
		setEndPosX(500);
		
		setStartPosY(350);
		setMidPosY(350);
		setEndPosY(1300);
*/		
}
	// Generates a random number between args "low" and "high"
	private int randomizer(int low, int high) {

		if(high == 1) { return 1; } // thats called a ball
		 
		Random r = new Random();
		int R = r.nextInt((high-low)+1)+low;

		return R;		

	}
	private void setPath1() {
		setStartPosX(1050);
		setMidPosX(randomizer(300,500));
		setEndPosX(randomizer(300,600));
		
		setStartPosY(350);
		setMidPosY(350);
		setEndPosY(1300);
	}
	private void setPath2() {
		setStartPosX(1050);
		setMidPosX(randomizer(200,500));
		setEndPosX(500);
		
		setStartPosY(350);
		setMidPosY(350);
		setEndPosY(1300);
	}
	private void setPath3() {
		setStartPosX(1050);
		setMidPosX(600);
		setEndPosX(500);
		
		setStartPosY(350);
		setMidPosY(350);
		setEndPosY(1300);
	}
	
	// X positions
	public void setStartPosX(int pos) 	{ startPosX = pos;	}
	public int getStartPosX() 			{ return startPosX; }

	public void setMidPosX(int pos) 	{ midPosX = pos; 	}
	public int getMidPosX() 			{ return midPosX;	}

	public void setEndPosX(int pos) 	{ endPosX = pos;	}
	public int getEndPosX() 			{ return endPosX;	}
	
	// Y positions
	public void setStartPosY(int pos) 	{ startPosY = pos;	}
	public int getStartPosY() 			{ return startPosY; }

	public void setMidPosY(int pos) 	{ midPosY = pos; 	}
	public int getMidPosY() 			{ return midPosY;	}

	public void setEndPosY(int pos) 	{ endPosY = pos;	}
	public int getEndPosY() 			{ return endPosY;	}
}
